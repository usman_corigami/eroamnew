$("#project").autocomplete({
        minLength: 2,
        source: countries,
        focus: function(event, ui) {
            $("#project").val(ui.item.label);
            return false;
        },
        select: function(event, ui) {

            var displayVal = ui.item.label;
            $("#project").val(displayVal);

            $('#searchValType').val(ui.item.type);
            $('#searchVal').val(ui.item.value);
            $('#countryId').val(ui.item.cid);
            $('#countryName').val(ui.item.cname);
            $('#countryRegion').val(ui.item.rid);
            $('#countryRegionName').val(ui.item.rname);

            $('#countryId').val(ui.item.value);
            $('#countryRegion').val(ui.item.rid);
            $('#countryRegionName').val(ui.item.rname);

            $('#starting_country').val('');
            $('#starting_city').val('');

            $('#destination_country').val('');
            $('#destination_city').val('');
            //$('#search_itinery_tour').prop('disabled',false);
            return false;
        },
        response: function(event, ui) {
            if (ui.content.length == 0) {
                project_search_cnt = ui.content.length;
            } else {
                project_search_cnt = 1;
            }
        }
    }).autocomplete("instance")._renderItem = function(ul, item) {
        return $("<li>")
            .append("<div>" + item.label + "</div>")
            .appendTo(ul);
    };

$("#start_location").autocomplete({
        minLength: 2,
        source: projects,
        focus: function(event, ui) {
            $("#start_location").val(ui.item.label);
            return false;
        },
        select: function(event, ui) {
            var displayVal = ui.item.label + ', ' + ui.item.desc;
            $("#start_location").val(displayVal);
            var option = $(".radio_tailor:checked").val();
            if (option == "packages" || option == "manual") {
                $('#starting_country').val(ui.item.cid);
                $('#starting_city').val(ui.item.value);
            } else {
               $('#starting_country').val(ui.item.cid);
                $('#starting_city').val(ui.item.value);
            }
            $('input[name="start_location"]').valid();
            return false;
        },
        response: function(event, ui) {
            if (ui.content.length == 0) {
                start_location_search_cnt = ui.content.length;
            } else {
                start_location_search_cnt = 1;
            }
        }
    }).autocomplete("instance")._renderItem = function(ul, item) {
        return $("<li>")
            .append("<div>" + item.label + ", " + item.desc + "</div>")
            .appendTo(ul);
    };

$("#end_location").autocomplete({
        minLength: 2,
        source: projects,
        focus: function(event, ui) {
            $("#end_location").val(ui.item.label);
            return false;
        },
        select: function(event, ui) {
            var displayVal = ui.item.label + ', ' + ui.item.desc;
            $("#end_location").val(displayVal);
            $("#destination_city").val(ui.item.value);
            $("#destination_country").val(ui.item.cid);
            $('input[name="end_location"]').valid();
            return false;
        },
        response: function(event, ui) {
            if (ui.content.length == 0) {
                end_location_search_cnt = ui.content.length;
            } else {
                end_location_search_cnt = 1;
            }
        }
    }).autocomplete("instance")._renderItem = function(ul, item) {
        return $("<li>")
            .append("<div>" + item.label + ", " + item.desc + "</div>")
            .appendTo(ul);
    };

$(document).ready(function() {
    $("#start_location").val('');
    $("#end_location").val('');
    var showReset = $('#resetDone').val();
    var resetSuccess = $('#resetSuccess').val();
    if (showReset == 1 || resetSuccess == 1 ) {
        $('#loginmodal').modal('show');
    }
});

var sform = $("#search-form");
sform.validate({
    ignore: [],
    submitHandler: function(form) { 
        $(".loader").css('display','block'); 
        form.submit();
    },
    errorPlacement: function (label, element) {
        if(element.hasClass('tour_checkbox')){
            label.insertAfter(element.closest('span').next('small'));
        }
        else{
            label.insertAfter($(element).parent('div'));
        }
        //$('#search_itinery_tour').attr("disabled", "disabled");
    },
    rules: {
        project:{
            required: true,
            projectAutocomplete: true
        },
        'tour_type[]':{
            required:true,
            maxlength: 2
        }
    }
});

jQuery.validator.addMethod("projectAutocomplete", function(value, element) {
    return this.optional(element) || (!$("#searchVal").val().trim() == '');
}, "Please select from dropdown."); 

jQuery.validator.addMethod("startAutocomplete", function(value, element) {
    return this.optional(element) || (!$("#starting_city").val().trim() == '');
}, "Please select from dropdown."); 

jQuery.validator.addMethod("endAutocomplete", function(value, element) {
    return this.optional(element) || (!$("#destination_city").val().trim() == '');
}, "Please select from dropdown."); 


jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Please enter letters only."); 

jQuery.validator.addMethod("unique", function (value, element, options) {
    // get all the elements passed here with the same class
    var elems = $(element).parents('form').find(options[0]);
    // the value of the current element
    var valueToCompare = value;
    // count
    var matchesFound = 0;
    // loop each element and compare its value with the current value
    // and increase the count every time we find one
    jQuery.each(elems, function () {
        thisVal = $(this).val();
        if (thisVal == valueToCompare) {
            matchesFound++;
        }
    });
    // count should be either 0 or 1 max
    if (this.optional(element) || matchesFound <= 1) {
        //elems.removeClass('error');
        return true;
    } else {
        //elems.addClass('error');
    }
}, jQuery.format("Please enter a Unique Value."))

jQuery.validator.addMethod("maxAge", function(value, element, max) {

    if(value != '03-05-1985'){
        var today = new Date();
        var day = today.getDate();
        var month = today.getMonth()+1;

        if(day >= 1 && day <= 9) { day = '0'+day;}
        if(month >= 1 && month <= 9) { month = '0'+month;}
        //var currentDate = day+"-"+month+"-"+today.getFullYear();
        var birthDate = value.split("-");
        var age = today.getFullYear() - birthDate[2];
        value = birthDate[2]+'-'+birthDate[1]+'-'+birthDate[0];
        var currentDate = today.getFullYear()+"-"+month+"-"+day;
        //console.log(Date.parse(value1) +' > '+ Date.parse(currentDate1));
        //console.log(today.getMonth()+'//'+value+'//'+age+'//'+currentDate+'//'+birthDate+'//'+max+'//'+age);
        
        if(Date.parse(value) > Date.parse(currentDate)) { return false; }
        if (age <= max) { return true;  }

        var m = month - birthDate[1];
        if (m < 0 || (m === 0 && day < birthDate[0])) { age--; }
        //console.log(m+'//'+age);
        return age <= max;
        
    } else {
        return true; 
    }
}, "Please enter valid Date of Birth.");

$('#search-form2').on('submit', function(event) {
    // adding rules for inputs with class 'comment'
    $('.firstname').each(function () {
        $(this).rules("add", {
            required: true,
            lettersonly:true,
            minlength: 2,
            maxlength: 15,
            unique : ['.firstname']
        });
    });

    $('.lastname').each(function () {
        $(this).rules("add", {
            required: true,
            lettersonly:true,
            minlength: 2,
            maxlength: 28
        });
    });     

    $('.dobdp').each(function () {
        $(this).rules("add", {
            required: true,
            maxAge: 17
        });
    });              
});

$("#search-form2").validate({
	submitHandler: function(form) { 
        $(".loader").css('display','block');    
        form.submit();
    },
    rules: {
        end_location:{
            required: true,
            endAutocomplete: true
        },
        start_location: {
            required: function(element) {
                if($("input[class='radio_tailor']:checked").val() != undefined) {
                    if($("input[class='radio_tailor']:checked").val() == 'auto') {
                        return true;
                    }else{
                        return false;
                    }
                }else{
                  return true;
                }
            },
            startAutocomplete: true
        },
        start_date: {
            required: true
        },
    },
    errorPlacement: function (label, element) {
        label.insertAfter($(element).parent('div'));
    }
});

$(document).on('change','.radio_tailor', function(){
	$('.location_input').nextAll().remove();
});

/*$(document).on('change', '.num_of_adult', function(){
 var total_adult = 0;
 $('.num_of_adult').map(function(){              
     total_adult = total_adult + parseFloat(this.value);
 }).get();

 if(total_adult <= 14)
 {
     $('#number_of_adults').val(total_adult);
 }
 else
 {
     var lastValue = $('#number_of_adults option:last-child').val();            
     if(lastValue != 14)
     {
         $("#number_of_adults option[value='"+lastValue+"']").remove();
     }
     $('#number_of_adults').append($("<option selected></option>").attr("value",total_adult).text(total_adult));
 }

 var selected_val = $(this).val();
 $("#total_room_adults").val(total_adult);
});*/

/*$(document).on('change','#number_of_adults', function(){
 var selected_val = $(this).val();
 $("#total_room_adults").val(selected_val);
});

$(document).on('change','#number_of_child', function(){
 var selected_val = $(this).val();
 $("#total_room_child").val(selected_val);
});*/

/*$(document).on('change', '.num_of_child ', function(){
 var total_adult = 0;
 $('.num_of_child').map(function(){              
     total_adult = total_adult + parseFloat(this.value);
 }).get();
 $("#total_room_child").val(total_adult);
 if(total_adult <= 6)
 {
     $('#number_of_child').val(total_adult);
 }
 else
 {
     var lastValue = $('#number_of_child option:last-child').val();            
     if(lastValue != 6)
     {
         $("#number_of_child option[value='"+lastValue+"']").remove();
     }
     $('#number_of_child').append($("<option selected></option>").attr("value",total_adult).text(total_adult));
 }
});*/

/* Added by Rekha Patel at 14/09/2018 - START */ 
$(document).on('change', '.room_select', function(){
    var total_room = $(this).val();
    var html = '';
    var i,total_pax,select;
    var adults = parseInt(0);
    var pax = parseInt(9);
    var pax1 = parseInt(9);
    var j = 0;
    var alertMsg = false;
    var roomSelect = 1;

    $('.numOfPax').each(function () { 
        j++;
        if($(this).attr('disabled') != 'disabled'){
            adults = adults + parseInt($(this).val());  
        }
    });

    if(adults <= 9){
        for(i = 1 ; i <= total_room ; i++){
            if(pax > 0){
                html += '<div id="roomDetail_'+i+'">';
                html += '<div class="row"><div class="col-sm-3 col-xl-2"><div class="lable_title bold_font"><i class="fa fa-user"></i> '+roomLabel+' '+i+'</div>';
                html += '<div class="row"><div class="col-sm-12 col-12"><div class="form-group"><div class="fildes_outer"><label>'+NumbePAXLabel+'</label><div class="custom-select"><select name="num_of_pax[]" id="num_of_pax_'+i+'" class="numOfPax">';

                total_pax = 1;
                if ($("#num_of_pax_"+i).length){ total_pax = $("#num_of_pax_"+i).val(); } 
                 
                for(k=1 ; k<=pax1; k++){
                    if(total_pax == k){ select = "selected"; } else { select = '';}
                    html += '<option value="'+k+'" '+select+' >'+k+'</option>';
                }

                if(total_pax > 1) { 
                    pax1 = pax1 - parseInt(total_pax); 
                }
                pax = pax - parseInt(total_pax);

                html += '</select></div></div></div></div></div></div>';
                html += '<div class="col-sm-9 col-xl-10 "><div class="paxDetail">';
                html += addPax(i,total_pax);
                html += '</div></div></div></div>';
            } else {
                if(alertMsg == false){ roomSelect = i-1; } 
                alertMsg = true;    
            }
        }

        $(".itinery_person_info").html('');    
        $(".itinery_person_info").append(html);
        
        if(alertMsg == true){
           alert('Please reduce number of PAX...'); 
           $('.room_select>option[value="'+(roomSelect)+'"]').prop('selected', true);
        }
    } else{
        $('.room_select>option[value="'+j+'"]').prop('selected', true);
        alert('Please reduce number of PAX...');
    }
    updateAdultChildCount();
});

$(document).on('change', '.numOfPax', function(){
    var total_pax = $(this).val();
    var pax_id = $(this).attr("id");
    var id = pax_id.split("_");
    id = id[3];

    html = addPax(id,total_pax);
    $("#roomDetail_"+id).find(".paxDetail").html(html); 

    var pax = 9;// - total_pax;
    var num_of_adults = parseInt(0);
    var j = 0;
    var select_pax;
    $('.numOfPax').each(function () { 
        j++;
        var currentId = $(this).attr('id');
        currentId = currentId.replace("num_of_pax_", "");

        if(currentId != id && currentId > id){
            if(pax > 0){
                select_pax = $(this).val();
                if(select_pax > 1) { pax = pax - parseInt(select_pax); }

                $(this).empty(); 

                for(var k = 1; k<= pax; k++){
                    if(select_pax == k){ select = "selected"; } else { select = '';}
                    $(this).append('<option value="'+k+'" '+select+' >'+k+'</option>');    
                }  
                //$("#roomDetail_"+currentId).find("input").attr("disabled", false);
                //$(this).attr("disabled", false);
            } //else {
                //if(pax < 1){
                    //$("#roomDetail_"+currentId).find("input").attr("disabled", true);
                    //$(this).attr("disabled", true);
                //}
            //}
        } else {
            if(parseInt($(this).val()) > parseInt(1)){ pax = parseInt(pax) - $(this).val(); }
        }

        num_of_adults += parseInt($(this).val());
        if(num_of_adults == 9){ $('.room_select>option[value="'+j+'"]').prop('selected', true);}
    });

    updateAdultChildCount();
});

//Show/Hide Child Age Datepicker
$(document).on('click', '.isChild', function(){
    var birthdate = '03-05-1985';
    var dob = new Date(birthdate);
    var today = new Date();
    var dayDiff = Math.ceil(today - dob) / (1000 * 60 * 60 * 24 * 365);
    var age = parseInt(dayDiff);

    var d = new Date();
    var day = d.getDate();
    if(day >= 1 && day <= 9) { day = '0'+day;}
    var month = d.getMonth()+1;
    if(month >= 1 && month <= 9) { month = '0'+month;}
    var currentDate = (day-1)+"-"+month+"-"+d.getFullYear();

    var pax = $(this).parents('div').parents('div').attr("id");
    var id = pax.split("_");
    id = id[1]+'_'+id[2];

    if($(this).prop('checked') == true) {
        $("#"+pax).children( "div:eq(1)").children("div:eq(0), div:eq(1)").removeClass("col-sm-6").addClass("col-sm-4");
        $("#"+pax).children( "div:eq(1)").children("div:eq(2)").show();
        $("#dob_"+id).val('');
        $("#age_"+id).val(0);
    } else {
        $("#"+pax).children( "div:eq(1)").children("div:eq(0), div:eq(1)").removeClass("col-sm-4").addClass("col-sm-6");
        $("#"+pax).children( "div:eq(1)").children("div:eq(2)").hide();
        $("#dob_"+id).val(birthdate);
        $("#age_"+id).val(age);
    } 
    updateAdultChildCount();
});

//Add No Of Pax List
function addPax(id,total_pax){
    var birthdate = '03-05-1985';
    var dob = new Date(birthdate);
    var today = new Date();
    var dayDiff = Math.ceil(today - dob) / (1000 * 60 * 60 * 24 * 365);
    var age = parseInt(dayDiff);
    var i;
    var html = '';
    for(i = 1 ; i <= total_pax ; i++){

        var firstname = '';
        var lastname = '';

        if($("#age_"+id+'_'+i).length > 0) { 
            var firstname =  $("input[name='firstname["+id+"]["+i+"]']").val();
            var lastname =  $("input[name='lastname["+id+"]["+i+"]']").val();
        }

        if($("#isChild_"+id+'_'+i).length > 0 &&  $("#isChild_"+id+'_'+i).prop('checked') == true) { 
            age = $("#age_"+id+'_'+i).val();
            dob = $("#dob_"+id+'_'+i).val();

            html += '<div id="paxDetail_'+id+'_'+i+'"><div class="lable_title"><span class="bold_font mr-4"><i class="fa fa-user"></i>'+PAXLabel+' '+i+'</span><span class="custom_check">'+Under_18Label+'<strong> </strong> <input type="checkbox" name="is_child['+id+']['+i+']" class="isChild" id="isChild_'+id+'_'+i+'" value="1" checked="checked"><span class="check_indicator">&nbsp;</span></span><small class="question"><i class="ic-faq"></i> </small></div><div class="row">';
            html += '<div class="col-sm-4 col-6" ><div class="form-group"><div class="fildes_outer"><label>'+firstNameLabel+'</label><input type="text" class="form-control firstname" placeholder="'+firstNameLabel+'" name="firstname['+id+']['+i+']" value="'+firstname+'" ></div></div></div>';
            html += '<div class="col-sm-4 col-6" ><div class="form-group"><div class="fildes_outer"><label>'+lastNameLabel+'</label><input type="text" class="form-control lastname" placeholder="'+lastNameLabel+'" name="lastname['+id+']['+i+']" value="'+lastname+'" ></div></div></div>';
            html += '<div class="col-sm-4"><div class="form-group"><div class="fildes_outer"><label>'+dobLabel+'</label><div class="input-group"><input id="dob_'+id+'_'+i+'" name="dob['+id+']['+i+']" type="text" data-date-end-date="0d" value="'+dob+'" placeholder="'+dobLabel+'" class="form-control dobdp" autocomplete="off" readonly><span class="input-group-addon px-2 py-2"><i class="ic-calendar"></i></span></div></div><label for="dob_'+id+'_'+i+'" generated="true" class="error" style="display:none;"></label></div></div>';
            html += '<input type="hidden" value="'+age+'" name="age['+id+']['+i+']" id="age_'+id+'_'+i+'">';
            html += '</div></div>'; 
        } else {
            html += '<div id="paxDetail_'+id+'_'+i+'"><div class="lable_title"><span class="bold_font mr-4"><i class="fa fa-user"></i>'+PAXLabel+' '+i+'</span>';
            //html += '<span class="custom_check">'+Under_18Label+'<strong> </strong> <input type="checkbox" name="is_child['+id+']['+i+']" class="isChild" id="isChild_'+id+'_'+i+'" value="1"><span class="check_indicator">&nbsp;</span></span><small class="question"><i class="ic-faq"></i> </small>';
            if(i == 1) { html += '<input type="hidden" name="is_child['+id+']['+i+']" class="isChild" id="isChild_'+id+'_'+i+'" value="0">'; }
            else { 
                html += '<span class="custom_check">'+Under_18Label+'<strong> </strong> <input type="checkbox" name="is_child['+id+']['+i+']" class="isChild" id="isChild_'+id+'_'+i+'" value="1"><span class="check_indicator">&nbsp;</span></span>';
                html += '<small class="question" data-toggle="tooltip" data-placement="bottom" data-original-title="'+pilotLabelt+')" style="cursor: no-drop;" ><i class="ic-faq" style="cursor: no-drop;"></i> </small>';
            }
            html += '</div><div class="row">';
            html += '<div class="col-sm-6 col-6" ><div class="form-group"><div class="fildes_outer"><label>'+firstNameLabel+'</label><input type="text" class="form-control firstname" placeholder="'+firstNameLabel+'" name="firstname['+id+']['+i+']"  value="'+firstname+'" ></div></div></div>';
            html += '<div class="col-sm-6 col-6" ><div class="form-group"><div class="fildes_outer"><label>'+lastNameLabel+'</label><input type="text" class="form-control lastname" placeholder="'+lastNameLabel+'" name="lastname['+id+']['+i+']" value="'+lastname+'" ></div></div></div>';
            html += '<div class="col-sm-4" style="display:none;"><div class="form-group"><div class="fildes_outer"><label>'+dobLabel+'</label><div class="input-group"><input id="dob_'+id+'_'+i+'" name="dob['+id+']['+i+']" type="text" data-date-end-date="0d" value="'+birthdate+'" placeholder="'+dobLabel+'" class="form-control dobdp" autocomplete="off" readonly><span class="input-group-addon px-2 py-2"><i class="ic-calendar"></i></span></div></div><label for="dob_'+id+'_'+i+'" generated="true" class="error" style="display:none;"></label></div></div>';
            html += '<input type="hidden" value="'+age+'" name="age['+id+']['+i+']" id="age_'+id+'_'+i+'">';
            html += '</div></div>';   
        }
    }
    return  html;  
}

//Update Adult & chiuld Count
function updateAdultChildCount(){
    var d = new Date();
    var day = d.getDate();
    if(day >= 1 && day <= 9) { day = '0'+day;}
    var month = d.getMonth()+1;
    if(month >= 1 && month <= 9) { month = '0'+month;}
    var currentDate = day+"-"+month+"-"+d.getFullYear();

    var audult = parseInt(0);
    var child = parseInt(0);
    var j=0;
    $('.isChild').each(function () { 
        if($(this).prop('checked') == true){
            child++;
            var pax = $(this).parents('div').parents('div').attr("id");
            var id = pax.split("_");
            id = id[1]+'_'+id[2];
            //$("#dob_"+id).val(currentDate);
            $("#dob_"+id).datepicker({format: 'dd-mm-yyyy', autoclose: true, todayHighlight: true,  startDate : '-18y +1d', endDate : '-1d'});
        } else {
            audult++;
        }

        j++;
        if(j > 9){
            var paxId = $(this).parents('div').parents('div').attr("id");
            var id = paxId.split("_");
            $("#"+paxId).remove();
            var pax = $("#roomDetail_"+id[1]).find(".paxDetail >div").length;

            if(pax > 0){
                $("#num_of_pax_"+id[1]).empty(); 
                for(var k = 1; k<= pax; k++){
                    if(pax == k){ select = "selected"; } else { select = '';}
                    $("#num_of_pax_"+id[1]).append('<option value="'+k+'" '+select+' >'+k+'</option>');    
                } 
            } else {
                $("#roomDetail_"+id[1]).remove();
            }
        }
    });

    $("#total_room_adults").val(audult);
    $("#total_room_child").val(child);
}

/*$('body').on('focus',".dobdp", function(){
    var d = new Date();
    var day = d.getDate();
    if(day >= 1 && day <= 9) { day = '0'+day;}
    var month = d.getMonth()+1;
    if(month >= 1 && month <= 9) { month = '0'+month;}
    var currentDate = day+"-"+month+"-"+d.getFullYear();

    $(this).val(currentDate);

    $(this).datepicker({
        format: 'dd-mm-yyyy', autoclose: true, todayHighlight: true, setDate: '+1d',
        onSelect: function(dateText, inst) {
            var date = $(this).val();
            var time = $('#time').val();
            alert('on select triggered');
            //$("#start").val(date + time.toString(' HH:mm').toString());

        }
    });
});*/


/* Added by Rekha Patel at 17/09/2018 - END */ 

/*$(document).on('change', '.children_append', function(){
 var div_name = $(this).attr("data-id");
 var total_child = $(this).val();
 child_html = '';
 if(total_child == 0)
 {
     $(".child_age_"+div_name).hide();
 }
 else
 {
     $(".child_age_"+div_name).show();
 }

 $(".child_append_"+div_name).html('');
 
 for(i = 1 ; i <= total_child ; i++)
 {
     var index_val = div_name-1;
     var id = '['+index_val+']['+i+']';
     var template = jQuery.validator.format($.trim($("#addChild").html()));
     $(template(id)).appendTo(".child_append_"+div_name);
     $('.netChild').each(function () {
         $(this).rules("add", {
             required: true
         });
     });
 }
});*/

/*$(document).on('change', '.child_select', function(e){
 
 var total_child = $(this).val();
 child_html = '';
 
 if(total_child == 0)
 {
     $(".children-block").hide();
 }
 else
 {
     $(".children-block").show();
     $(".children_selectbox").html('');
     var j = 1;
     for(i = 1 ; i <= total_child ; i++)
     {
         var id = '[0]['+i+']';
         var template = jQuery.validator.format($.trim($("#addChildAlone").html()));
         $(template(id)).appendTo(".children_selectbox");
         $('.netChild').each(function () {
             $(this).rules("add", {
                 required: true
             });
         });
         e.preventDefault();                
     }
 }
});*/