$(document).ready(function() {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
});

var tommorowDate = new Date();
tommorowDate.setDate(tommorowDate.getDate() + 1);
var previousDate;
$(".searchDate").datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
    startDate: tommorowDate,
    toggleActive:false,
})
// Save date picked
.on('show', function () {
    previousDate = $(this).val();
})
// Replace with previous date if no date is picked or if same date is picked to avoide toggle error
.on('hide', function () {
    if ($(this).val() === '' || $(this).val() === null) {
        $(this).val(previousDate).datepicker('update');
    }
});
$(".searchDate").datepicker("setDate", '+1d');
var getDateEnd = $('#getDateEnd').val();
$(".datepicker1").datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
});

$(".dob_picker").datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true,
    endDate: new Date()
});

 $('.open-datepicker').click(function(){
       $(document).ready(function(){
           $(".datepicker").datepicker().focus();
           $(".datepicker1").datepicker().focus();
           $(".searchDate").datepicker().focus();
       });
   });

$.validator.addMethod("verifyEmail",
    function(value, element) {
        var result = false;
        var token = $('#token').val();
        $.ajax({
            type:"POST",
            async: false,
            url: "/email/validate", // script to validate in server side
            data: {"reg_email": value,'_token':token},
            success: function(data) {
                var final_result = '';
                if(data.success == 1)
                {
                    if(data.social == 1)
                    {
                        final_result = false;
                    }
                    else
                    {
                        final_result = true;    
                    } 
                }
                else
                {
                    final_result = false;
                }
                result = final_result;
            }
        });
        // return true if username is exist in database
        return result;
        alert("RESULT "+result);
    },
    
);

$.validator.addMethod("checkEmail",
    function(value, element) {
        var result = false;
        var token = $('#token').val();
        $.ajax({
            type:"POST",
            async: false,
            url: "/email/validate", // script to validate in server side
            data: {"reg_email": value,"_token":token},
            success: function(data) {
                var final_result = '';
                if(data.success == 1)
                {
                    if(data.social == 1)
                    {
                        final_result = true;
                    }
                    else
                    {
                        final_result = false;    
                    } 
                }
                else
                {
                    final_result = true;
                }
                result = final_result;
            }
        });
        // return true if username is exist in database
        return result;
        alert("RESULT "+result);
    },
    
);

$.validator.addMethod("pwcheck", function(value) {
    // return /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/.test(value) 
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w]))(?=.*\d).{8,}.+$/.test(value) 
}, "Must be at least 8 characters, and ideally contain some or all of: CAPS and lower case, numbers, random characters like #!*");

$("#signup_form_model").validate({
    rules: {           
        reg_email: {
            required: true,
            email:true,
            verifyEmail : true
        },
        reg_pass: {
            required: true,
            minlength: 8,    
            pwcheck : true, 
        }
    },        
    messages: {
        reg_email: {
            verifyEmail: "Email already exists"
        }
    },
    errorPlacement: function (label, element) {
        label.insertAfter($(element).parent('div'));
    },
    errorClass : "error_group",
    submitHandler: function (form) {
        var registerForm = $("#signup_form_model");
        var formData = registerForm.serialize();
        $( '#email-error' ).html( "" );
        $( '#password-error' ).html( "" );

        $.ajax({
            url:'/signup',
            type:'POST',
            data:formData,
            beforeSend: function() {
                $('input#submitSignupForm').val('Please Wait..');
                $('input#submitSignupForm').prop('type', 'button');
            },
            success:function(data) { 
                $('input#submitSignupForm').val('Submit');
                $('input#submitSignupForm').prop('type', 'submit');
                if(data.errors) {                    
                    if(data.errors.email){
                        $( '#email-error' ).html( data.errors.email );
                    }
                    if(data.errors.password){
                        $( '#password-error' ).html( data.errors.password );
                    }                    
                }
                if(data.success) {
                    var logindata = {};
                    logindata._token = $("input[name='_token']").val();
                    logindata.username_login = $("#reg_email").val();
                    logindata.password_login = $("#reg_pass").val();
                    eroam.ajax('post', 'login', logindata, function(response) {
                        if (response.trim() == 'valid') {
                            window.location.href = '/profile/step1';
                        }
                    });
                }
            },
        });
    }
});

$("#login_form").validate({
    rules: {           
        username_login: {
            required: true,
            email:true
        },
        password_login: {
            required: true,
        }
    },
    errorPlacement: function (label, element) {
        label.insertAfter($(element).parent('div'));
    },
    errorClass : "error_group",
    submitHandler: function (form) {
        //$("submitLoginForm").value('Loading..');
        $('input.submitLoginForm').val('Loading');
        var postData = $("#login_form").serialize();
        eroam.ajax('post', 'login', postData, function(response) {
            $('#login-btn').html('Loading...');
            if (response.trim() == 'valid') {
                $('#login-error-msg').hide();
                $('#login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');                        
                if($("#currentPath").val() === 'payment-summary') {
                    window.location.reload();
                } else {
                    window.location.href = '/profile/step1';
                }
            }else if(response.trim() == 'confirm_first'){
                $('#login-error-message').text('Please Confirm your mail first.').show();
                $('#login-btn').html('Log In');
				setTimeout(function(){ $("#login-error-message").fadeOut(); },5000);
            }else {
                $('#login-error-message').text('Invalid Email Address or Password').show();
                $('#login-btn').html('Log In');
				setTimeout(function(){ $("#login-error-message").fadeOut(); },5000);
            }
        }, function() {
            $('#login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking User...');
        });
    }
});

$("#forgot_form").validate({
    rules: {           
        user_email: {
            required: true,
            email:true,
            checkEmail : true
        },
    },        
    messages: {
        user_email: {
            checkEmail: "We Could Not Find Your Email Address."
        }
    },
    errorPlacement: function (label, element) {
        label.insertAfter($(element).parent('div'));
    },
    errorClass : "error_group",
    submitHandler: function (form) {
        
        var forgotForm = $("#forgot_form");
        var formData = forgotForm.serialize();
   
        $.ajax({
            url:'/forgot-password',
            type:'POST',
            data:formData,
            beforeSend: function() {
                $('input#submitForgotForm').val('Please Wait..');
                $('input#submitForgotForm').prop('type', 'button');
            },
            success:function(data) { 
                $('input#submitForgotForm').val('Continue');
                $('input#submitForgotForm').prop('type', 'submit');
                //$.unblockUI();
                if(data.errors){                    
                    if(data.errors.email){
                        $( '#email-error' ).html( data.errors.email );
                    }
                    if(data.errors.password){
                        $( '#password-error' ).html( data.errors.password );
                    }                    
                }
                if(data.success) {
                    $(".forgot_message").show();
                    $("#user_email").val('');
                    $(".forgot_message").html('Check your mail for recover password link.');
					setTimeout(function(){ $('div#forgotModal .forgot_message').fadeOut(); },5000);
                }
            },
        });
    }
});

$("#login_form2").validate({

    rules: {
        username_login: {
            required: true,
            email:true
        },
        password_login: {
            required: true,
        }
    },
    errorPlacement: function (label, element) {
        label.insertAfter($(element).parent('div'));
    },
    errorClass : "error_group",
    submitHandler: function (form) {
        
        $('input.submitLoginForm').val('Loading');
        var postData = $("#login_form2").serialize();
            eroam.ajax('post', 'login', postData, function(response) {
                $('#login-btn2').html('Loading...');
                if (response.trim() == 'valid') {
                    $('#login-error-msg2').hide();
                    $('#login-btn2').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');
                    window.location.href = '/review-itinerary/';
                }
                else if(response.trim() == 'confirm_first')
                {
                    $('#login-error-message2').text('Please Confirm your mail first.').show();
                    $('#login-btn2').html('Log In');
                }
                else {
                    $('#login-error-message2').text('Invalid Email Address or Password').show();
                    $('#login-btn2').html('Log In');
                }
            }, function() {
                $('#login-btn2').html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking User...');
            });
        }
});

$('#checkbox-reg').click(function (){
    if($('#checkbox-reg').is(':checked') == true){
        $("#reg_pass").prop("type", "text");           
    }else{
        $("#reg_pass").prop("type", "password");            
    } 
});
