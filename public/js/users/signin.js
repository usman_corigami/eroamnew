$(window).load(function() {

    $(".input_effects input").focusout(function() {
        if ($(this).val() != "") {
            $(this).addClass("has-content");
        } else {
            $(this).removeClass("has-content");
        }
    });
    $(".input_effects input").each(function() {
        if ($(this).val() != "") {
            $(this).addClass("has-content");
        } else {
            $(this).removeClass("has-content");
        }
    });

});
$(document).ready(function() {
	var facebookBtn = $('#fb-login-btn');
	var loginForm = $('#login-form1');
	var fbResponse;
	var fields = {
		fields: 'email, picture.width(500).height(500), first_name, last_name'
	};

	$('.login-group input').keypress(function(e) {
		if (e.which == 13) {
			$('#login-btn').click();
		}
	});
	
    $("#login-form1").validate({
        rules: {
            username: {
                 required: true,
                 email:true
            },
            password: {
                required: true
            },

        },
        messages: {

            username: {
                required: "Please enter Email Address."
            },
            password: {
                required: "Please enter Password."
            },

        },
        errorPlacement: function (label, element) {
        	label.insertAfter(element);
            // label.appendTo(element.parent('.form-group'));
        },
        submitHandler: function (form) {
			login();
		}
    });


    jQuery.validator.addMethod("lettersonly", function(value, element) {
			  return this.optional(element) || /^[a-z]+$/i.test(value);
			}, "Please enter letters only."); 
            $("#signup_form").validate({
                rules: {
                    first_name: {
                        required: true,
                        lettersonly: true
                    },
                    last_name: {
                        required: true,
                        lettersonly: true
                    },
                    email: {
                        required: true,
                        email:true
                    },
                    password: {
                        required: true
                    },
                    confirmpassword: {
                       equalTo : "#reg_password"
                    },
                    nationality: {
                        required: true
                    },
                    gender: {
                        required: true
                    },
                    age_group: {
                        required: true
                    },
                    currency: {
                        required: true
                    },
                    policy:{
                        required:true
                    },
                },
                messages: {

                    first_name: {
                        required: "Please enter First Name."
                    },
                    last_name: {
                        required: "Please enter Last Name."
                    },
                    email: {
                        required: "Please enter Email Address."
                    },
                    password: {
                        required: "Please enter Password."
                    },
                    confirmpassword: {
                        equalTo: 'Password and Confirm Password must be same.'
                    },
                    nationality: {
                        required: "Please select Nationality.",
                    },
                    gender: {
                        required: "Please select Gender.",
                    },
                    age_group: {
                        required: "Please select Age Group.",
                    },
                    currency: {
                        required: "Please select Currency.",
                    },
                    policy: {
                        required: "Please select checkbox to agree privacy policy and tems of use.",
                    }
                },
                errorPlacement: function (label, element) {
                    label.insertAfter(element);
                },
                submitHandler: function (form) {

                    form.submit();

                }
            });
	// LOGIN USER
	function login(){

		var username = $("#username").val();
		var password = $("#password").val();
		// var postData = $('#login-form1').serialize();
		
			eroam.ajax('post', 'login', {username_login:username,password_login:password}, function(response) {
				if (response.trim() == 'valid') {
					//$('#login-error-msg').hide();
					$('#login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');

					window.location.href = '/profile/step1';
					//return false;
				} else {
					$('#login-error-msg').text('Invalid Email Address or Password').show();
					$('#login-btn').html('Log In');
				}
			}, function() {
				$('#login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking User...');
			});
	}
	

	// GET FACEBOOK SCRIPT
	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/en_US/sdk.js', function() {
		FB.init({
			appId: '1731928143731970',
			version: 'v2.7'
		});

		FB.getLoginStatus(function(response) {
			if (response.status == 'connected') {
				FB.api('/me', fields, function(response) {
					fbResponse = response;
					eroam.api('post', 'check-fb-user', {email: fbResponse.email}, function(response) {
						if (response) {
							facebookBtn.html('<i class="fa fa-facebook"></i> Continue as '+ fbResponse.first_name + '<img src="'+fbResponse.picture.data.url+'">');
						}
					});
				});
			}
			facebookBtn.removeAttr('disabled');
		});
	});

	// ON CLICK FACEBOOK BUTTON
	facebookBtn.on('click', function(e) {
		e.preventDefault();
		FB.login(function(response) {
			var grantedScopes = response.authResponse.grantedScopes.split(',');
			var hasEmail = grantedScopes.some(function(v, k) {
				return v == 'email';
			});
			if (response.status == 'connected') {
				// CHECK IF USER GRANTED PERMISSION TO HIS/HER EMAIL
				if (hasEmail == true) {
					FB.api('/me', fields, function(response) {
						if (response.email) {
							fbResponse = response;
							checkFacebookUser(response);
						} else {
							$('#fb-error-msg').text('Please validate your Facebook email in order to continue.').show();
						}
					});
				} else {
					$('#fb-error-msg').text('You must allow eRoam\'s permission to read your Facebook email address to proceed.').show();
				}
			}
		}, { scope: 'email', return_scopes: true });
	});

	// FACEBOOK USER SIGN UP FOR EROAM
	loginForm.delegate('#sign-up-fb-btn', 'click', function(e) {
		e.preventDefault();
		var name = fbResponse.first_name + ' ' + fbResponse.last_name;
		var fname = fbResponse.first_name;
		var lname = fbResponse.last_name;
		var email = fbResponse.email;
		var password = $('#fb-password').val();
		var currency = $('#fb-currency').val();

		// VALIDATION
		if (name == '' || fname == '' || lname == '' || email == null || password == '' || currency == null) {
			$('#fb-sign-up-error').text('All fields are required.').show();
		} else {
			if (password.length < 6) {
				$('#fb-sign-up-error').text('Password must be at least 6 characters long.').show();
			} else {
				eroam.api('post', 'user/check-email-availability', {email: email}, function(response) {
					if (response) {
						$('#fb-sign-up-error').text('Your Facebook email address ('+email+') is already in use.').show();
					} else {
						// STORE USER
						$('#fb-sign-up-error').hide();
						var data = {
							email: email,
							first_name: fname,
							last_name: lname,
							password: password,
							currency: currency,
							image_url: fbResponse.picture.data.url,
						};
						eroam.api('post', 'user/fb-signup', data, function(response) {
							if (response) {
								eroam.ajax('post', 'login', {username: email, password: password}, function(response) {
									if (response == 'valid') {
										window.location.href = '/profile/step1';
									}
								}, function() {
									$('#sign-up-fb-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');
								});
							}
						}, function() {
							$('#sign-up-fb-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Processing...');
						});
					}
				});
			}
		}
	});
});

function checkFacebookUser(fbUser) {
	if (fbUser.error) {
		window.location.href = '/login';
	}

	eroam.api('post', 'check-fb-user', {email: fbUser.email}, function(response) {
		if (response) {
			eroam.ajax('post', 'login', {username: response.email}, function(response) {
				if (response == 'valid') {
					window.location.href = '/profile/step1';
				}
			}, function() {
				$('#fb-login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');
			});
		} else {
			var fbEmail = fbUser.email ? fbUser.email : '';
			var currencies = JSON.parse($('#all-currencies').val());
			var currencyHtml = '';

			for (var i = 0; i < currencies.length; i++) {
				currencyHtml += '<option value="'+currencies[i].code+'">'+currencies[i].code+' '+currencies[i].name+'</option>';
			}

			$('#login-form').html(
				'<h4>Sign up for eRoam</h4>' +
				'<div class="form-group">'+
					'<input type="password" id="fb-password" class="form-control" placeholder="Choose a Password">' +
					'<i class="fa fa-lock"></i>' +
				'</div>' +
				'<div class="form-group">'+
					'<select class="form-control" id="fb-currency">' +
						'<option disabled selected value="">Select Currency</option>' +
						currencyHtml +
					'</select>'+
					'<i class="fa fa-dollar"></i>' +
				'</div>' +
				'<p id="fb-sign-up-error" class="error-msg" style="display: none"></p>' +
				'<button id="sign-up-fb-btn"><i class="fa fa-facebook"></i> Sign up as '+ fbUser.first_name +' ' + fbUser.last_name +' <img src="' + fbUser.picture.data.url + '"></button>'
			);
		}
	}, function() {
		$('#fb-login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking User...');
	});
}