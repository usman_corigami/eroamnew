<?php

namespace App\Service\Mystifly;

/**
 * 
 */
class AirRevalidate
{
	
	public function getAirRevalidate($fareSourceCode){
        
        $pathHelper = resolve('PathHelper');
        $api_url = $pathHelper->getApiPath();
        $domain = $pathHelper->getDomain();

        $result = array();

        if($fareSourceCode){

            $data = array('FareSourceCode'=>trim($fareSourceCode));
            $request = TRUE;
            while( $request ){
                try{
                    $client = new \GuzzleHttp\Client(['headers' => ['domain' => $domain]]);
                    $response = $client->request('post',$api_url . 'mystifly/airRevalidate', [
                        'form_params' => $data
                    ]);
                    $request = FALSE;
                }catch(Exception $e){
                    Log::error('An error has occured during a guzzle call on CacheController@set for provider "'.$provider.'" '.'with key "'.$key.'" and ERROR MESSAGE:'.$e->getMessage() );
                }
            }
                    
            $result = json_decode( $response->getBody() , true );
        }
        return $result;
    }
}