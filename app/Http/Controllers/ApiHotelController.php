<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Client;

use Carbon\Carbon;
use App\City;
use App\HotelRoomType;
use App\Hotel;

use App\Currency;
use App\HotelPrice;
use App\Country;
use DB;

class ApiHotelController extends Controller {

	public function hotel_by_city_id_v2()
	{	
		$data      = Input::all();
		//$data ='{"city_ids":["7"],"date_from":"2018-09-16","date_to":"2018-09-19","traveller_number":"1","hotel_category_id":[null],"room_type_id":[null],"domain":"localeroam.com"}';
		//$data = json_decode($data,true);
		//$data['city_ids'] = $data['city'];//City::where('name',$data['city'])->value('id');

		//$data['city_ids'] = array(772);
		//$data['date_from'] = '2017-11-30';
		//$data['date_from'] = '2017-12-02';
		//$data['traveller_number'] = '1';
		//dd($data);
		$hotel_ids = HotelPrice::where('allotment','>','0')->pluck('hotel_id');//dd($hotel_ids);
		$price_ids = HotelPrice::where('allotment','>','0')->pluck('id');
		$hotels    = Hotel::whereIn('id', $hotel_ids);
		
        //dd($hotels);
		// if(isset($data['hotel_category_id']) && count($data['hotel_category_id']) > 0){
		// 	$hotels = $hotels->whereIn('hotel_category_id',$data['hotel_category_id']);
		// }
		if(!isset($data['room_type_id']) || count($data['room_type_id']) == 0 ){
			$room_type = [];
			switch ($data['traveller_number']) {//check the number of traveller then set default room type
				case '1':
					$room_type = HotelRoomType::where('pax',1)->orWhere('is_dorm',1)->get();
					break;
				case '2':
					$room_type = HotelRoomType::where('pax',2)->orWhere('is_dorm',1)->get();
					break;
				case '3':
					$room_type = HotelRoomType::where('pax',3)->orWhere('is_dorm',1)->get();
					break;
				default:
					$room_type = HotelRoomType::where('pax','>=',$data['traveller_number'])->get();
					break;	
			}

			if($room_type){
				foreach ($room_type as $key => $value) {
					if(!isset($data['gender'])){
						
						$data['room_type_id'][] = $value['id'];//filter for form exclude dorm requires female only
					}
					
					if(isset($data['gender']) && strtolower($data['gender']) =='male' && $value['female_only'] == 0){
						
						$data['room_type_id'][] = $value['id'];//filter for form exclude dorm requires female only
					}

					if(isset($data['gender']) && strtolower($data['gender']) =='female'){
						
						$data['room_type_id'][] = $value['id'];//filter for form exclude dorm requires female only
					}
				}	
			}
			
			
		}

		
		$hotels = $hotels->orderBy('hotel_category_id','asc');	
		$nValue =$data['traveller_number'];
		//print_r($data['city_ids']);exit;

		//$nCityId = explode(',',$data['city_ids']);
		//DB::enableQueryLog();
		$hotels = $hotels->whereIn('city_id', $data['city_ids'])
			->where('zhotel.is_publish','=', 1)
			->orderBy('ranking', 'ASC')
			->with(['city', 'currency', 'image', 'category',
				'price' => function($query) use ($price_ids,$nValue)
				{
					$query->where('max_pax','>=',$nValue)	
						->with(['room_type',
							'season' => function($query)
							{
								$query->with('currency', 'supplier');
							}
						]);
				}
			])->get();
			//dd(DB::getQueryLog());
		//echo "<pre>";print_r($hotels[0]);exit;
		$result = array();
		foreach($hotels as $hotel_key => $hotel)
		{	
			$temp_price = [];
			//echo "<pre>";print_r($hotel);exit;
			$price_count = count($hotel->price);
			$hotel->provider = 'eroam';

			if( $hotel->image == NULL ) // if image is empty then append uploads/no-image.png
			{
				array_push($hotel->image, 'uploads/no-image.png');
			}
			foreach($hotel->price as $price_key => $price)
			{	
			
				
				if(!isset($price->season->hotel_supplier_id) || $price->season->hotel_supplier_id != $hotel->default_hotel_supplier_id)
				{

					$price_count--;

					unset($hotel->price[$price_key]); 
					// remove price/room if the supplier is not the hotel's default supplier
				}else{
					//filter season jayson added
					if((strtotime($price->season->from) > strtotime($data['date_from']) && strtotime($price->season->to) > strtotime($data['date_from'])) || (strtotime($price->season->from) < strtotime($data['date_to']) && strtotime($price->season->to) < strtotime($data['date_to']))  ){
						//echo "11";exit;
						
						$price_count--;
						unset($hotel->price[$price_key]); 
						
					}
					// else if(isset($data['traveller_number']) && $price['room_type']['pax'] < $data['traveller_number']){

					// 	$price_count--;
					// 	unset($hotel->prices[$price_key]); 
					// }
					else if(isset($data['room_type_id']) && !in_array(null, $data['room_type_id']) && count($data['room_type_id']) > 0 && !in_array($price->hotel_room_type_id, $data['room_type_id'])){
						//echo $data['room_type_id']."22";exit;
						$price_count--;
						unset($hotel->price[$price_key]);

					}else{
						//echo "33";exit;

							array_push($temp_price, $price); 
							// $temp_price[] = $price;
	
					}

				}



				// added property to determine if filter hotel_room_type_id is set;
				if(!empty($data['room_type_id']))
				{
					$price->is_room_type_filtered = ($data['room_type_id'] == $price->hotel_room_type_id) ? 1: 0;
				}
				else
				{
					$hotel->is_room_type_filtered = 0;
				}
			}

			if($price_count > 0) // remove hotel if there is no price/room
			{   
				//echo "44";exit;
				unset($hotel->price) ;
				$hotel->price =  $temp_price;
				array_push($result, $hotel);

			}
			// added property to determine if filter hotel_category_id is set;
			if(!empty($data['category_id']))
			{
				$hotel->is_category_filtered = ($data['category_id'] == $hotel->hotel_category_id) ? 1: 0;
			}
			else
			{
				$hotel->is_category_filtered = 0;
			}
			
		}
		//echo "<pre>";print_r($result);exit;
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);

	}
}