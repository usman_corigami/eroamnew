@extends('itinenary.booking-layout')

@section('booking-content')
@php
   $leg_detail = session()->get('search');
  // echo '<pre>'; print_r($leg_detail);exit;

    $aPassengerInfo = session()->get( 'search_input' )['pax'];
    $aPassengerData = array();
    $aChildData = array();
    foreach($aPassengerInfo as $roomPassenger)
    {
        foreach($roomPassenger as $aPassenger)
        {
            if($aPassenger['child'] == 0)
                $aPassengerData[] = $aPassenger;
            else
                $aChildData[] = $aPassenger;
        }

    }
    $oCountries = $oCountries['country'];
    $total = $totalCost * $travellers;
    $adults = !empty (session()->get('search_input')['num_of_adults']) ?array_sum(session()->get('search_input')['num_of_adults']):0;
    $children = !empty(session()->get('search_input')['num_of_children']) ? array_sum(session()->get('search_input')['num_of_children']):0;
    if(!empty($adults) && !empty($children)){
        $total = (($adults  + $children) *$totalCost);
    }if(!empty($adults) && empty($children)){
         $total = (($adults) *$totalCost);
    }if(empty($adults) && !empty($children)){
         $total = (($children) *$totalCost);
    }
    //$finalTotal = number_format($total + $gst, 2, '.', ',');
    $finalTotal = number_format($finalTotalCost + $gst,2,'.',',');
    $eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
    $travellers   = session()->get( 'search' )['travellers'];
    $total_childs = session()->get( 'search' )['child_total'];
    $rooms = session()->get( 'search' )['rooms'];
    $search_input = session()->get('search_input');
    $transports = array_column(session()->get( 'search' )['itinerary'],'transport');
    $provider = array_column($transports,'provider');

    $dynamicData=array_keys($transports[0]['passangerArray'][0]['questions']);	
    $qstArr=['dob','government_id','gender','nationality','profession','marital_status','outbound_breakfast','outbound_lunch','outbound_supper','return_breakfast','return_lunch','return_supper'];
    $payload=[];
        foreach($qstArr as $key=>$val){
            if(in_array($val,$dynamicData)){
                array_push($payload,$val);
            }
        }
        foreach($payload as $key=>$val)
                    {
                            switch($val)
                            {                             
                                case 'government_id':                               
                                $government_id=1;
                                break;
                                case 'marital_status':                               
                                $marital_status=1;
                                break;
                                case 'outbound_breakfast':                              
                                $meal=1;
                                break;
                                case 'outbound_lunch':                               
                                $meal=1;
                                break;
                                case 'outbound_supper':                              
                                $meal=1;
                                break;
                                case 'return_breakfast':
                                $meal=1;
                                break;
                                case 'return_lunch':
                                $meal=1;
                                break;
                                case 'return_supper':
                                $meal=1;
                                break;
                            }

                    }
    
    $tSelect = 0;
    if(isset($transports) && !empty($transports)){
        $transports = json_decode(json_encode($transports) , true );
    }
       
    foreach($transports as $transport)
    {
        if(isset($transport['PricedItineraries']) && isset($transport['PricedItineraries']['PricedItinerary']))
        {
            if(isset($transport['PricedItineraries']['PricedItinerary']['RequiredFieldsToBook']))
            {
                $tSelect = 1; break;
            }
        
            else{  
                if(isset($transport['PricedItineraries'])){
                    $checkInfo = array_column($transport['PricedItineraries']['PricedItinerary'] ,'RequiredFieldsToBook');
                    foreach($checkInfo as $info)
                    {
                        if(isset($info['RequiredFieldsToBook']))
                           $tSelect = 1; break;
                    }
                }
            }
        }
    }
@endphp
@push('style')
<style type="text/css">
.disable_links{
  cursor: no-drop;
}</style>
@endpush
<div class="itinerary_right">
    @include('itinenary.partials.sidebarstrip')
   
    <div class="container-fluid">
        @include('itinenary.partials.payment-filter-option')

        <div class="itinerary_page pt-2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class=" m-1">
                            <div class="tripDetails mb-3">
                                <div class="card panel border-0 rounded-0 p-3">
                                    <div class="row">
                                        <div class="col-sm-7 col-xl-8">

                                            @if(!empty($data['search_input']['option']) && ($data['search_input']['option'] == 'auto'))
                                                @if(!empty($data['itinerary']) && (count($data['itinerary'])>1) )
                                                <h5 class="font-weight-bold">Multi-City Tailormade Auto</h5>
                                                @else
                                                <h5 class="font-weight-bold">Single-City Tailormade Auto</h5>
                                                @endif
                                            @else
                                                <h5 class="font-weight-bold">Manual Itinerary</h5>
                                            @endif
                                            <div class="media">
                                                <i class="ic-calendar mr-2"></i>
                                                <div class="media-body pb-3 mb-0">
                                                    {{$startDate}} - {{$endDate}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-xl-4">
                                            <div class="row align-items-end">
                                                <div class="col-xl-9 col-sm-8 text-left text-sm-right">
                                                    <ul class="list-unstyled mb-0">
                                                        <li class="list-inline-item">
                                                            <i class=" ic-local_hotel"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="ic-local_activity"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="ic-directions_bus"></i>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-xl-3 col-sm-4 text-left text-sm-center mt-2 mt-md-0 border-left">
                                                    <h2 class="font-weight-bold mb-0">{{$totalDays}}</h2>
                                                     <?php echo ($totalDays>1) ? 'Days' :'Day'; ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" m-1">

                <div class="row">
                    <div class="col-12">
                        <h4>Passenger Information</h4>
                        <form  action="{{ url('payment') }}" method="post" name="signup_form" id="signup_form_id" >
                        {{ csrf_field() }}  
                        <div class="accordion" id="accordionExample">
                            @foreach($aPassengerData as $nKey=> $aPassenger )
                            <div class="card card_new continue_guest mb-4">
                                <div class="px-3 py-2 heading border-bottom" id="headingOne">
                                    <div class="heading_title" data-toggle="collapse" data-target="#collapse{{ $nKey }}" aria-expanded="{{ $nKey== 0 ? 'true' : 'false' }}" aria-controls="collapseOne">
                                        <div class="row">
                                            <div class="col-sm-10 col-md-10 col-lg-11"> Passenger {{$nKey+1}}</div>

                                            <div class="plusbtns col-sm-1" >

                                                <a href="#" class="float-right true rounded-circle"><span> _ </span> </a>

                                                <a href="#" class="float-right false rounded-circle"><span>+</span>  </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="collapse{{$nKey}}" class="collapse {{ $nKey== 0 ? 'show' : '' }}" aria-labelledby="headingOne" data-parent="#accordionExample">

                                    <div class="card-body">

                                        <div class="row mb-4">
                                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2 mb-2 mb-xl-0">
                                                <div class="fildes_outer">
                                                    <label>Title. <span class="required">*</span></label>
                                                    <div class="custom-select">
                                                        <select class="passenger_title" name="passenger_title[{{$nKey}}]">
                                                            <option value="">Select</option>
                                                            <option value="Mr">Mr</option>
                                                            <option value="Ms">Ms</option>
                                                            <option value="Mrs">Mrs</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2 mb-2 mb-xl-0">

                                                <div class="fildes_outer">
                                                    <label>First Name <span class="required">*</span></label>
                                                    <input type="text" class="form-control passenger_first_name"  name="passenger_first_name[{{$nKey}}]" placeholder="John" value="{{ $aPassenger['firstname'] }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2 mb-2 mb-xl-0">
                                                <div class="fildes_outer">
                                                    <label>Last Name <span class="required">*</span></label>
                                                    <input type="text" class="form-control passenger_last_name"  name="passenger_last_name[{{$nKey}}]" placeholder="Last Name" value="{{ $aPassenger['lastname'] }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-2 mb-xl-0">

                                                <div class="fildes_outer">
                                                    <label>Date of Birth <span class="required">*</span></label>
                                                    <input type="text" class="form-control dob"  name="passenger_dob[{{$nKey}}]"  placeholder="DD-MM-YYYY" readonly>
                                                </div>
                                            </div>
                                           
                                            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-2 mb-xl-0">

                                                <div class="fildes_outer">
                                                    <label>Email <span class="required">*</span></label>
                                                    <input type="text" class="form-control email"  placeholder="Email" name="passenger_email[{{$nKey}}]">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-4">

                                            <div class="col-sm-6 col-md-6 col-lg-2 mb-2 mb-lg-0">

                                                <div class="fildes_outer">
                                                    <label>Phone <span class="required">*</span></label>
                                                    <input type="text" class="form-control phone"  placeholder="Phone" name="passenger_contact_no[{{$nKey}}]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-2 mb-2 mb-lg-0">

                                                <div class="fildes_outer">
                                                    <label>Area Code <span class="required">*</span></label>
                                                    <input type="text" class="form-control new_area"  placeholder="Area Code" name="area_code[{{$nKey}}]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-2 mb-2 mb-lg-0">
                                                <div class="fildes_outer">
                                                    <label> Gender</label>
                                                        <input type="text" readonly name="gender[{{$nKey}}]" class="form-control passenger_gender">
                                                </div>
                                                
                                            </div>
                                            @if(in_array('mystifly',$provider))
                                            <div class="col-sm-6 col-md-6 col-lg-3 mb-2 mb-lg-0">

                                                <div class="fildes_outer">
                                                    <label>Meal Preference </label>
                                                    <div class="custom-select">
                                                    {{Form::select('passenger_meal['.$nKey.']',$aMealType['meal'],'',['class'=>'form-control passenger_meal'])}}
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col-sm-6 col-md-6 col-lg-3 mb-2 mb-lg-0">

                                                <div class="fildes_outer">
                                                    <label>Country <span class="required">*</span></label>
                                                    <div class="custom-select">
                                                        <select name="country[{{$nKey}}]" class="form-control country passenger_country">
                                                            <option>Select Country</option>
                                                            @foreach($oCountries as $aCountry)
                                                                <option value="{{ $aCountry['code'] }}" data-id ='{{ $aCountry['country_code'] }}'>{{ $aCountry['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                        <input type="hidden" class="form-control area country_code"  placeholder="Area code" name="country_code[{{$nKey}}]">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                       {{-- Start for busbud  passenger--}}
                                       @if(in_array('busbud',$provider))
                                       <div class="row mb-4">
                                        @if(isset($government_id))
                                        <div class="col-sm-6 col-md-6 col-lg-2 mb-2 mb-lg-0">
                                            <div class="fildes_outer">
                                                <label>Government Id <span class="required">*</span></label>
                                                <div class="custom-select">
                                                    <select class="government_id" name="government_id[{{$nKey}}]">
                                                        <option value="">Select</option>
                                                        <option value="passport">Passport</option>
                                                        <option value="dni">Dni</option>
                                                        <option value="national_picture_id">National Picture Id</option>
                                                        <option value="foreigners_card">Foreigners Card</option>
                                                        <option value="birth_certificate">Birth Certificate</option>
                                                        <option value="military_card">Military Card</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @if(isset($marital_status))
                                        <div class="col-sm-6 col-md-6 col-lg-2 mb-2 mb-lg-0">
                                            <div class="fildes_outer">
                                                <label>Marital status <span class="required">*</span></label>
                                                <div class="custom-select">
                                                    <select class="marital_status" name="marital_status[{{$nKey}}]">
                                                        <option value="">Select</option>
                                                        <option value="married">Married</option>
                                                        <option value="divorced">Divorced</option>
                                                        <option value="single">Single</option>
                                                        <option value="widowed">Widowed</option>
                                                       
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @if(isset($meal))
                                        <div class="col-sm-6 col-md-6 col-lg-2 mb-2 mb-lg-0">
                                            <div class="fildes_outer">
                                                <label>Meal Preference <span class="required">*</span></label>
                                                <div class="custom-select">
                                                    <select class="meal" name="meal[{{$nKey}}]">
                                                        <option value="">Select</option>
                                                        <option value="vegetarian">Vegetarian</option>
                                                        <option value="beef">Beef</option>
                                                        <option value="chicken">Chicken</option>
                                                        <option value="child_meal">Child Meal</option>
                                                        <option value="spicy">Spicy</option>
                                                        <option value="snack">Snack</option>
                                                        <option value="vegetarian_snack">Vegetarian Snack</option>
                                                        <option value="anything">Anything</option>                                                     
                                                    </select>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        @endif
                                    </div>
                                      @endif
                                        {{-- End busbud passenger--}}
                                        @if($tSelect == 1)
                                        <div class="row mb-4">

                                            <div class="col-sm-6 col-md-6 col-lg-3 mb-2 mb-lg-0">

                                                <div class="fildes_outer">
                                                    <label>Passport Country <span class="required">*</span></label>
                                                    <div class="custom-select">
                                                        <select name="passport_country[{{$nKey}}]" class="form-control country">
                                                            <option>Select Country</option>
                                                            @foreach($oCountries as $aCountry)
                                                                <option value="{{ $aCountry['code'] }}">{{ $aCountry['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-3 mb-2 mb-lg-0">
                                                <div class="fildes_outer">
                                                    <label>Passport Expiry Date <span class="required">*</span></label>
                                                    <input type="text" name="passport_date[{{$nKey}}]" class="form-control passport_date"  placeholder="Passport Expiry Date" readonly>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-6 col-lg-3 mb-2 mb-lg-0">
                                                <div class="fildes_outer">
                                                    <label>Passport No <span class="required">*</span></label>
                                                    <input type="text" name="passport_no[{{$nKey}}]" class="form-control passport_no" placeholder="Passport No">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-3 mb-2 mb-lg-0">
                                                <div class="fildes_outer">
                                                    <label>Postcode <span class="required">*</span></label>
                                                    <input type="text" name="postcode[{{$nKey}}]" class="form-control postcode" placeholder="Postcode">
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="row">
                                            <div class="col-12 mb-2">
                                                <div class="fildes_outer">
                                                    <label>Special Notes</label>
                                                    <textarea name="special_note[{{$nKey}}]" rows="4" class="form-control special_note"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group black-checkbox">
                                           <span class="custom_check">Please send all communication to this traveler<input type="radio" name="location-select" value="{{$nKey}}" {{($nKey == 0 ) ? 'checked' : ''}} class="radio_tailor" id="auto"><span class="check_indicator">&nbsp;</span></span>
                                       </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <?php $nId = $nKey; ?>
                            @foreach($aChildData as $nKey => $aPassenger )
                            <div class="card card_new continue_guest mb-4">
                                <div class="px-3 py-2 heading border-bottom" id="headingOne">
                                    <div class="heading_title">
                                        <div class="row">
                                            <?php $nId = $nId+1;?>
                                            <div class="col-sm-11 col-md-10 col-xl-11"> Child {{ $nKey+1 }}</div>
                                            <div class="plusbtns col-sm-1" data-toggle="collapse" data-target="#collapse{{ $nId }}" aria-expanded="false" aria-controls="collapseOne">

                                                <a href="#" class="float-right true rounded-circle"><span> _ </span> </a>

                                                <a href="#" class="float-right false rounded-circle"><span>+</span>  </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="collapse{{$nId}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">

                                    <div class="card-body">

                                        <div class="row mb-4">
                                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2 mb-2 mb-xl-0">
                                                <div class="fildes_outer">
                                                    <label>Title. <span class="required">*</span></label>
                                                    <div class="custom-select">
                                                        <select class="child_title" name="child_title[{{$nKey}}]">
                                                            <option value="">Select</option>
                                                            <option value="Mr">Master</option>
                                                            <option value="Ms">Miss</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2 mb-2 mb-xl-0">

                                                <div class="fildes_outer">
                                                    <label>First Name <span class="required">*</span></label>
                                                    <input type="text" class="form-control child_first_name"  name="child_first_name[{{$nKey}}]" placeholder="John" value="{{ $aPassenger['firstname'] }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-2 mb-2 mb-xl-0">

                                                <div class="fildes_outer">
                                                    <label>Last Name <span class="required">*</span></label>
                                                    <input type="text" class="form-control child_last_name"  name="child_last_name[{{$nKey}}]" placeholder="Last Name" value="{{ $aPassenger['lastname'] }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-3 mb-2 mb-xl-0">

                                                <div class="fildes_outer">
                                                    <label>Date of Birth <span class="required">*</span></label>
                                                    <input type="text" class="form-control child_dob"  name="child_dob[{{$nKey}}]"  placeholder="DD-MM-YYYY" value="{{ $aPassenger['dob'] }}" readonly>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-4 col-md-4 col-lg-4 col-xl-3 mb-2 mb-xl-0">

                                                <div class="fildes_outer">
                                                    <label>Email </label>
                                                    <input type="text" class="form-control child_email"  placeholder="Email" name="child_email[{{$nKey}}]">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-4">

                                            <div class="col-sm-6 col-md-6 col-lg-2 mb-2 mb-lg-0">

                                                <div class="fildes_outer">
                                                    <label>Phone </label>
                                                    <input type="text" class="form-control child_phone"  placeholder="Phone" name="child_phone[{{$nKey}}]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-2 mb-2 mb-lg-0">

                                                <div class="fildes_outer">
                                                    <label>Area Code <span class="required">*</span></label>
                                                    <input type="text" class="form-control new_area"  placeholder="Area Code" name="child_area_code[{{$nKey}}]">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-2 mb-2 mb-lg-0">
                                                <div class="fildes_outer">
                                                    <label> Gender <span class="required">*</span></label>
                                                        <input type="text" readonly name="child_gender[{{$nKey}}]" class="form-control child_gender">
                                                </div> 
                                            </div>
                                            @if(in_array('mystifly',$provider))
                                            <div class="col-sm-6 col-md-6 col-lg-3 mb-2 mb-lg-0">

                                                <div class="fildes_outer">
                                                    <label>Meal Preference</label>
                                                    <div class="custom-select">
                                                    {{Form::select('child_meal['.$nKey.']',$aMealType['meal'],'',['class'=>'form-control child_meal'])}}
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col-sm-6 col-md-6 col-lg-3 mb-2 mb-lg-0">

                                                <div class="fildes_outer">
                                                    <label>Country <span class="required">*</span></label>
                                                    <div class="custom-select">
                                                        <select name="child_country[{{$nKey}}]" class="form-control country passenger_country">
                                                            <option>Select Country</option>
                                                            @foreach($oCountries as $aCountry)
                                                                <option value="{{ $aCountry['code'] }}" data-id ='{{ $aCountry['country_code'] }}'>{{ $aCountry['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                        <input type="hidden" class="form-control area country_code"  placeholder="Area code" name="child_country_code[{{$nKey}}]">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                       {{-- Start for busbud child  passenger--}}
                                       @if(in_array('busbud',$provider))
                                       <div class="row mb-4">
                                            @if(isset($government_id))
                                            <div class="col-sm-6 col-md-6 col-lg-3 mb-2 mb-lg-0">
                                                <div class="fildes_outer">
                                                <label>Government Id <span class="required">*</span></label>
                                                <div class="custom-select">
                                                    <select class="child_government_id" name="child_government_id[{{$nKey}}]">
                                                        <option value="">Select</option>
                                                        <option value="passport">Passport</option>
                                                        <option value="dni">Dni</option>
                                                        <option value="national_picture_id">National Picture Id</option>
                                                        <option value="foreigners_card">Foreigners Card</option>
                                                        <option value="birth_certificate">Birth Certificate</option>
                                                        <option value="military_card">Military Card</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @if(isset($meal))
                                        <div class="col-sm-6 col-md-6 col-lg-2 mb-2 mb-lg-0">
                                            <div class="fildes_outer">
                                                <label>Meal Preference <span class="required">*</span></label>
                                                <div class="custom-select">
                                                    <select class="child_meal" name="child_meal[{{$nKey}}]">
                                                        <option value="">Select</option>
                                                        <option value="vegetarian">Vegetarian</option>
                                                        <option value="beef">Beef</option>
                                                        <option value="chicken">Chicken</option>
                                                        <option value="child_meal">Child Meal</option>
                                                        <option value="spicy">Spicy</option>
                                                        <option value="snack">Snack</option>
                                                        <option value="vegetarian_snack">Vegetarian Snack</option>
                                                        <option value="anything">Anything</option>                                                     
                                                    </select>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        @endif
                                    </div>
                                     @endif
                                        {{-- End busbud child passenger--}}
                                        @if($tSelect == 1)
                                        <div class="row mb-4">
                                            <div class="col-sm-6 col-md-6 col-lg-3 mb-2 mb-lg-0">
                                                <div class="fildes_outer">
                                                    <label>Passport Country <span class="required">*</span></label>
                                                    <div class="custom-select">
                                                        <select name="child_passport_country[{{$nKey}}]" class="form-control country">
                                                            <option>Select Country</option>
                                                            @foreach($oCountries as $aCountry)
                                                                <option value="{{ $aCountry['code'] }}" data-id ='{{ $aCountry['country_code'] }}'>{{ $aCountry['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-3 mb-2 mb-lg-0">
                                                <div class="fildes_outer">
                                                    <label>Passport Expiry Date <span class="required">*</span></label>
                                                    <input type="text" name="child_passport_date[{{$nKey}}]" class="form-control child_passport_date"  placeholder="Passport Expiry Date" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-3 mb-2 mb-lg-0">
                                                <div class="fildes_outer">
                                                    <label>Passport No <span class="required">*</span></label>
                                                    <input type="text" name="child_passport_no[{{$nKey}}]" class="form-control child_passport_no" placeholder="Passport No">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-3 mb-2 mb-lg-0">
                                                <div class="fildes_outer">
                                                    <label>Postcode <span class="required">*</span></label>
                                                    <input type="text" name="child_postcode[{{$nKey}}]" class="form-control child_postcode" placeholder="Postcode">

                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="row mb-4">
                                            <div class="col-12">
                                                <div class="fildes_outer my-3">
                                                    <label>Special Notes</label>
                                                    <textarea name="child_special_note[{{$nKey}}]" rows="4" class="form-control child_special_note"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach                            
                            
                            <div class="card  continue_guest border-0 pl-2 pr-2 pt-2  mb-3">
                                <div class="p-2 heading border-bottom" id="headingThree">
                                    <div class="" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Payment
                                        <a href="#" class="float-right true rounded-circle"><span> _ </span>  </a>
                                        <a href="#" class="float-right false rounded-circle"><span>+</span>  </a>
                                    </div>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body pb-5">
                                        <h5 class="pb-3 ">Billing Contact Details</h5>

                                        <div class="row">

                                            <div class="col-sm-6 col-md-4 col-xl-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>First Name <span class="required">*</span></label>
                                                    <input type="text" name="billing_first_name" class="passenger_first_name form-control" value="{{ !empty($data['user'])?$data['user']->customer->first_name:'' }}" placeholder="First Name">

                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-4 col-xl-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Family name (As Shown on Passport) <span class="required">*</span></label>
                                                    <input type="text" name="billing_last_name" class="passenger_first_name form-control" value="{{ !empty($data['user'])?$data['user']->customer->last_name:'' }}" placeholder="Family Name">

                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-4 col-xl-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Date of Birth <span class="required">*</span></label>
                                                    <input name="billing_passenger_dob" type="text" placeholder="DD-MM-YYYY" class="form-control passenger_dob" readonly>
                                                    <span class="arrow_down"><i class="ic-calendar open-datepicker"></i> </span>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-4 col-xl-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Email Address <span class="required">*</span></label>
                                                    <input type="email" name="billing_email" class="passenger_email form-control" value="{{ !empty($data['user'])?$data['user']->customer->email:'' }}" placeholder="Email Address">

                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-4 col-xl-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Contact Number <span class="required">*</span></label>
                                                    <input type="text" name="billing_contact" class="passenger_contact_no form-control" value="{{ !empty($data['user'])?$data['user']->customer->contact_no:'' }}" placeholder="Contact Number">

                                                </div>
                                            </div>
                                        </div>


                                        <h5>Billing Address</h5>
                                        <div class="row">
                                            <div class="col-sm-6 col-md-4 col-xl-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Company Name (Optional)</label>
                                                    <input type="text" name="passenger_company_name" class="form-control" placeholder="Company Name">

                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-4 col-xl-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Street Address <span class="required">*</span></label>
                                                    <input type="text" name="passenger_address_one" class="form-control" placeholder="Street Address">

                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-4 col-xl-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Additional Address Information (Optional)</label>
                                                    <input type="text" name="passenger_address_two" class="form-control" placeholder="Additional Address Infomation">

                                                </div>
                                            </div>

                                            <div class="col-sm-6  col-md-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Suburb / Town <span class="required">*</span></label>
                                                    <input type="text" name="passenger_suburb" class="form-control passenger_suburb" placeholder="Suburb / Town">
                                                </div>
                                            </div>
                                            <div class="col-sm-6  col-md-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>State / Territory / Region <span class="required">*</span></label>
                                                    <input type="text"  name="passenger_state" class="form-control passenger_state" placeholder="State / Territory / Region">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Postcode / Area Code <span class="required">*</span></label>
                                                    <input type="text" name="passenger_zip" class="form-control passenger_zip" placeholder="Postcode / Area Code">
                                                </div>
                                            </div>


                                            <div class="col-sm-6 col-md-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Country <span class="required">*</span></label>
                                                    <div class="custom-select">
                                                        <div class="custom-select">
                                                            <select name="passenger_country[{{$nKey}}]" class="form-control country">
                                                                <option>Select Country</option>
                                                                @foreach($oCountries as $aCountry)
                                                                    <option value="{{ $aCountry['code'] }}" data-id ='{{ $aCountry['country_code'] }}'>{{ $aCountry['name'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="totalAmount" id="totalAmount" value="{{$finalTotal}}">
                                        <input type="hidden" name="currency" id="currency" value="{{$currency}}">
                                        <div class="row">
                                            <div class="col-sm-6 col-md-8 col-lg-9 col-xl-9">
                                                <h5 class="pb-2">Payment Method</h5>

                                                <div class="form-group black-checkbox mt-3">
                                                    <span class="custom_check custom-check-error">I agree to the terms and conditions. &nbsp; <input type="checkbox" checked name="terms" class="terms" id="terms" value="1"><span class="check_indicator">&nbsp;</span></span> <a href="javascript:void(0)" class="blue disable_links">View Terms and Conditions here.</a>
                                                </div>

                                                <div class="form-group black-checkbox mt-3">
                                                    <span class="custom_check">This package pricing is inclusive of flights &nbsp; <input type="checkbox" id="checkbox-04" value="3" checked><span class="check_indicator">&nbsp;</span></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3">
                                                <img src="{{  url( 'images/visa.jpg') }}" />
                                                <img src="{{  url( 'images/mastercard.jpg') }}" />

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-md-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Credit Card Number <span class="required">*</span></label>
                                                    <input type="text" name="card_number" id="card_number" class="form-control" placeholder="XXXX-XXXX-XXXX-XXXX">
                                                    <input type="hidden" id="card_valid">
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-2 col-lg-2 col-xl-2 form-group">
                                                <div class="fildes_outer">
                                                    <label>Month <span class="required">*</span></label>
                                                    <div class="custom-select" >
                                                        <select name="month" id="month" data-stripe="exp_month" class="month">
                                                            <option value="">MM</option>
                                                            <?php $expiry_month = date('m');?>
                                                            <?php for($i = 1; $i <= 12; $i++) {
                                                            $s = sprintf('%02d', $i);?>
                                                            <option value="<?php echo $s;?>" <?php //if ( $expiry_month == $i ) { ?>  <?php //} ?>><?php echo $s;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-2 col-lg-2 col-xl-2 form-group">
                                                <div class="fildes_outer">
                                                    <label>Year <span class="required">*</span></label>
                                                    <div class="custom-select">
                                                        <select name="year" id="year" data-stripe="exp_year" class="year">
                                                            <option value="">YYYY</option>
                                                            <?php
                                                            $lastyear = date('Y')+21;
                                                            $curryear = date('Y');
                                                            for($k=$curryear;$k<$lastyear;$k++){ ?>
                                                            <option value="<?php echo substr($k, -2);?>"><?php echo $k;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Security code / CSV <span class="required">*</span></label>
                                                    <input maxlength="3" name="cvv" id="cvv" type="text" placeholder="XXX" class="form-control">
                                                    <span class="arrow_down"><i class="fa fa-question-circle-o"></i> </span>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 form-group">
                                                <button type="submit" name=""  class="btn  btns_input_dark transform d-block w-100 btn_p">Pay ${{$currency}} {{$finalTotal}} NOW  </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="card disabled continue_guest border-0 pl-2 pr-2 pt-2  mb-3" data-toggle="tooltip" title="@lang('home.create_itenerary_checkbox3_title')">
                                <div class="p-2 heading border-bottom disabled" id="account">
                                    <div class="disabled" data-toggle="collapse" data-target="#accounts" aria-expanded="false" aria-controls="collapseThree">
                                        Account
                                        <a href="#" class="float-right true rounded-circle" style="cursor: not-allowed;"><span> _ </span>  </a>
                                        <a href="#" class="float-right false rounded-circle" style="cursor: not-allowed;"><span>+</span>  </a>
                                    </div>
                                </div>
                            

                            </div>
                        </div>
                         <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <table class="table mb-0">
                                        <tbody>
                                            <tr>
                                                <td class="border-0">

                                                    <div>  
                                                        <p style="cursor: pointer;"><a  href="javascript:void(0)" class="blue disable_item_custom">Save Itinerary</a>
                                                         </p>

                                                        <p> <a href="javascript:void(0)" class="blue disable_links">View Saved Itineraries</a></p>

                                                        <p> <a href="javascript:void(0)" class="blue disable_links">Enter Promo Code</a> </p>
                                                        <p> &nbsp; </p>

                                                        <p class="pt-2">Contact Us +<a href="javascript:void(0)" class="blue disable_links">61 (0)3 9999 6774 </a></p>

                                                    </div>
                                                </td>
                                                <td colspan="2" class="text-right border-0">
                                                    <!-- <p>Total Per Person</p> -->
                                                    <p>Sub Total Amount</p>
                                                    <p>Credit Card Fee 2.5%</p>
                                                    <p>Local Payment</p>
                                                    <p class="pt-2"><strong>Total Amount</strong></p>

                                                </td>

                                                <td class="text-right border-0">
                                                    <!-- <p>${{$currency}} {{number_format($totalCost, 2, '.', ',')}}</p> -->
                                                    <p>${{$currency}} {{number_format($finalTotalCost, 2, '.', ',')}}</p>
                                                    <p>${{$currency}} {{number_format($gst, 2, '.', ',')}}</p>
                                                    <p>${{$currency}} {{number_format($finalTotalCost,2, '.', ',')}}</p>

                                                    <p class="pt-2"><strong>${{$currency}} {{$finalTotal}}</strong></p>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12"> 
                                    I understand and agree that I am entering into an electronic transaction, and by clicking Continue, I also accept and agree to all terms of eRoam’s <a href="javascript:void(0)" class="blue disable_links">Sales and Refund Policy</a>. I agree to receive electronically all documents, including warranty, disclosure and/or insurance documents, as applicable, to my nominated email address. Your contact details are collected in case we need to contact you about your booking. If you do not complete your booking, then by clicking the ‘Continue’ button below you agree that we may contact you by email or by telephone (including by SMS) to follow up regarding your incomplete booking. For more information, please see our privacy policy. <a href="{{URL::to('privacy-policy')}}" class="blue" target="_blank">Click Here</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
   <script src="{{url('js/payform.min.js')}}"></script>
   <script src="{{url('js/itinerary/common.js')}}"></script>
   <script src="{{url('js/booking-summary.js')}}"></script>
   <script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
   <script type="text/javascript">
    $(document).ready(function(event) {
        // if flights are no longer available
         var isFlightAvailable = "{{$isFlightAvailable}}";
         if(isFlightAvailable == 1){
          
          $('.no-flight-summary').modal('show');
          
         }
        $('.passenger_first_name, .passenger_last_name,.email,.dob,.age,.phone,.new_area,.country,.gender,.passport_date,.passport_no,.child_title,.child_first_name,.child_last_name,.child_dob,.child_age,.child_country,.child_passport_date,.child_passport_no,.government_id,.child_government_id,.marital_status,.meal,.child_meal').each(function () { //,.dob,.age,.email,.phone,.area,.country,.gender,.meal,.passport_date,.special_note
            $(this).rules("add", {
                required: true,
            });
        });

        $('.passenger_country').change(function(){
            var countryCode =  $(this).find('option:selected').attr('data-id');
            $(this).siblings('.country_code').val(countryCode);
            console.log($(this).siblings('.country_code').val());
        });
        $('.child_title').change(function(){
            var data = $(this).find('option:selected').val();
            if(data == 'Mr')
                $(this).closest('.collapse').find('.child_gender').val('Male');
            else
                $(this).closest('.collapse').find('.child_gender').val('Female');

        });
        $('.passenger_title').change(function(){
            var data = $(this).find('option:selected').val();
            if(data == 'Mr')
                $(this).closest('.collapse').find('.passenger_gender').val('Male');
            else
                $(this).closest('.collapse').find('.passenger_gender').val('Female');

        });
        //debugger;
        //var todayDate = new Date();
        dt = new Date();
        dt.setFullYear(new Date().getFullYear() - 18);
        $('.dob,.passenger_dob').datepicker({endDate:dt,format: "dd-mm-yyyy"})
        .on('change', function(){
            $('.datepicker').hide();
        });

        $(".child_passport_date").datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
            startDate: '-3d',

        }).on('change', function(){
            $('.datepicker').hide();
        });
       /* $('.child_passport_date').datepicker({ minDate:0 }).on('change', function(){
            $('.datepicker').hide();
        });*/
        //$('.passport_date').datepicker({ minDate:todayDate });
        $('.passport_date').datepicker({
           format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true,
            startDate: '-3d',
       }).on('change', function(){
        $('.datepicker').hide();
        });

       @if($tSelect == 1)
            $('.passport_date,.passport_no,.postcode,.passport_country,.child_passport_date,.child_passport_no,.child_postcode,.child_passport_country').each(function () { //,.dob,.age,.email,.phone,.area,.country,.gender,.meal,.passport_date,.special_note
                $(this).rules("add", {
                    required: true,
                });
            });
        @endif
    });
   </script>
@endpush