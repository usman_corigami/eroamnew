@if(isset($leg['activities']) && !empty($leg['activities']))
    
  @foreach($leg['activities'] as $kact => $activity)
    @php 
    $j = 0;$adult = 0;$child =0;$intant = 0; 
    global $finalTotalCost;
  @endphp
    @if(isset($activity['tourGrades']['ageBands']))
      @foreach($activity['tourGrades']['ageBands'] as $age_band)
        @if($age_band['bandId'] == 1)
          <?php $adult = $age_band['count']; ?>
        @elseif($age_band['bandId'] == 2)
          <?php $child = $age_band['count']; ?>
        @elseif($age_band['bandId'] == 3)
          <?php $intant = $age_band['count']; ?>
        @endif
      @endforeach
    @else
    <?php 
      $adult = session()->get('search')['travellers'];
      $child = @session()->get('search')['child_total']; ?>
    @endif
    <?php 
    $pass_info = '';
    $adults = (isset($adult)) ? $adult : 0;
    $children = (isset($child)) ? $child : 0;
    $intant = (isset($intant)) ? $intant : 0;
    $pass_info = $adults." Adult";
    if($children != 0)
    {
      $pass_info .= ", ".$children." Child";
    }
    if($intant != 0)
    {                                              
      $pass_info .= ", ".$intant." Infant";
    }
    //dd($leg['activities']);
    ?>
    <tr>
        <td>
            <div class="tour_icon cityboxIcon"><i class="ic-local_activity"></i> </div>
            <div class="tour_info cityboxDetails">
                <strong>{{$activity['name']}}</strong>
                <p> {{date('d M Y', strtotime($activity['date_selected']))}}<br/>
                Duration: {{!empty($activity['duration1']) ? ucwords($activity['duration1']) : (!empty($activity['duration']) ? ucwords($activity['duration'])."Day" : '')}} (approx.) <br/>
                <?php //dd($activity['tourGrades']); 
                if(isset($activity['tourGrades']['gradeTitle']))
                {
                ?>
                Option : {{ @$activity['tourGrades']['gradeTitle'] }}
              <?php }?>
                </p>
            </div>
        </td>

        <td class="text-center">${{$currency}} {{ (isset($activity['tourGrades']['merchantNetPrice'])) ? number_format($activity['tourGrades']['merchantNetPrice'],2) : number_format($activity['price'][0]['price'],2)}}</td>
        <td class="text-center">
            <div class="price">
                {{ $pass_info }}
                <!-- <input class="form-control form-control-lg shadow-none rounded-0 text-center qty-inputControl disable_item_custom qty_textbox_width" type="text" readonly placeholder="" value="{{ $pass_info }}"></div> -->
        </td>
        <td>
            <div class="text-right">
                <strong> ${{$currency}} 
                {{ (isset($activity['tourGrades']['merchantNetPrice'])) ? number_format($activity['tourGrades']['merchantNetPrice'],2) : number_format($activity['price'][0]['price'],2)}}</strong><br>
                
                <a href="javascript:void(0)" class="disable_item_custom">Change Dates</a><br>
                <span class="cancellation_policy1">
                  @if(isset($activity['cancellation_policy']))
                  <a href="javascript:void(0)" data-target="#cancellationActivityPolicy{{$cj}}" data-toggle="modal">Cancellation Policy</a>
                  
                  @else
                   <a href="javascript:void(0)" class="disable_item_custom">Cancellation Policy</a>
                  @endif
                </span><br>
                <a href="javascript:void(0)" class="disable_item_custom">Remove From Itinerary</a>


            </div>
        </td>
    </tr>
    <tr>

        <td colspan="4" class="border-0">
            <form id="form-{{$activity['id']}}"  method="post">
                <div class="pl-5">                                                            
                    <?php 
                    $validate_weight = 0;
                    $hotelPickup = 0;
                    if(isset($activity['hotelPickup']) && $activity['hotelPickup'] == true) 
                    {
                        $hotelPickup = 1;
                    }
                    if(isset($activity['paxInfo']))
                    {?>
                        <div class="row">
                        <?php 
                        $i = 1;
                        foreach ($activity['paxInfo'] as $keys => $pax_info) 
                        {                             
                        
                        foreach ($activity['paxInfo'][$keys] as $pass) {

                            $ic = $i-1;
                            $fnm   = $activity["id"]."['".$keys."'][]['firstname']";
                            $lnm   = $activity["id"]."['".$keys."'][]['lastname']";
                            $age   = $activity["id"]."['".$keys."'][]['age']";
                            ?>
                            <input type="hidden" name="{{$fnm}}" class="pass-fnm-{{$keys}}-{{$activity['id']}}" value="{{@$pass['firstname']}}">
                            <input type="hidden" name="{{$lnm}}" class="pass-lnm-{{$keys}}-{{$activity['id']}}-{{$ic}}" value="{{@$pass['lastname']}}">
                            <input type="hidden" name="{{$age}}" class="pass-age-{{$keys}}-{{$activity['id']}}-{{$ic}}"  value="{{@$pass['age']}}">
                            <?php 
                           
                            if(isset($pass['bookingQuestions']))
                            {
                                $validate_weight = 1;
                                if(isset($pass['bookingQuestions']['answer']))
                                {

                                    $passbq = explode(" ",$pass['bookingQuestions']['answer']);
                                    $pass_we = $passbq[0];
                                    $pass_kg = $passbq[1];
                                    $option = '<option value="'.$pass_kg.'">'.$pass_kg.'</option>';
                                }
                                else
                                {
                                    $pass_we = '';
                                    $pass_kg = '';
                                    $option = '<option value="kg">kg</option><option value="lb">lb</option>';
                                }                            
                            }
                            else if(array_search('23', array_column($activity['bookingQuestions'], 'questionId')))
                            {
                                $pass_we = '';
                                $pass_kg = '';
                                $option = '<option value="kg">kg</option><option value="lb">lb</option>';
                            }
                        ?>
                        
                        <?php if(isset($pass_we))
                        {
                            $weight   = $activity["id"]."['".$keys."'][]['weight']"."[".$i."]";
                            if(empty($pass['age']))
                            {
                                $pass['age'] = 0;
                            }
                        ?> 
                            <div class="col-12 col-sm-6 col-md-3">
                                <div class="ml-2">
                                    <div class="row no-gutters">
                                        <div class="col-12">
                                            <strong class="title_sub"> Passenger {{$i}} ({{@$pass['age']}})</strong>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-6">
                                            <div class="fildes_outer">
                                                <label>Weight *</label>
                                                <input type="text" aria-describedby="emailHelp" placeholder="Weight" value="{{$pass_we}}" class="form-control weight-{{$keys}}-{{$activity['id']}}-{{$ic}}" data-questionID="23" type="text" name="{{$weight}}" required min="1" max="500" >
                                            </div>
                                            <label for="{{$weight}}" generated="true" class="error"></label>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-6">
                                            <div class="fildes_outer">
                                                <label></label>
                                                <div class="custom-select">
                                                    <select class="weight_select_{{$activity['id']}}" data-tourgrade-title="{{$activity['id']}}" data-questionID="23">
                                                        {!! $option !!}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                    
                        
                    <?php } $i++; } } ?>
                    </div>
                    <?php }?>
                    @if(isset($activity['hotelPickup']) && $activity['hotelPickup'] == true)
                        <?php 
                        $display = '';                                                                    
                        $hotelId = @$activity['hotelPickups']['hotelId'];
                                       
                        ?>

                        <div class=" mt-4 ml-2">
                            <div class="row">
                                <div class="col-12">
                                    <strong class="title_sub"> Pickup</strong>
                                </div>
                                @if(empty($activity['hotel_list']))
                                    <div class="col-12 col-sm-12 col-md-12 mt-3">
                                        <div class="fildes_outer">
                                            <label>Complimentary Pickup *</label>
                                            <input type="text" name="pickup-{{$activity['id']}}" class="form-control compli-pickup-{{$activity['id']}}" aria-describedby="emailHelp" placeholder="Complimentary Pickup" required value="{{@$activity['hotelPickups']['pickupPoint']}}">
                                        </div>
                                        <label for="pickup-{{$activity['id']}}" generated="true" class="error"></label>
                                    </div>
                                @else
                                    <div class="col-12 col-sm-3 col-md-3 mt-3">
                                        <div class="custom-control custom-radio">
                                            <input id="location_{{$activity['id']}}" name="location-select" type="radio" class="custom-control-input location-select location-select-{{$activity['id']}}" data-tourCode="{{$activity['id']}}" value="1" checked >
                                            <label class="custom-control-label" for="location_{{$activity['id']}}" <?php echo (isset($hotelId)) ? 'checked' : ""; ?>> Choose Location</label>
                                        </div>
                                    </div>

                                    <?php 
                                    $display = "none";
                                    if(isset($hotelId))
                                    {
                                        $display = "block";
                                    }

                                    ?>

                                    <div class="col-12 col-sm-3 col-md-9 choose_location_{{$activity['id']}}" style="display: <?php echo $display ?>">
                                        <div class="fildes_outer">
                                            <label>Locations</label>
                                            <div class="custom-select">
                                                <select name="pickup-{{$activity['id']}}" class="pickup-{{$activity['id']}} pickup-hotel" data-tourCode="{{$activity['id']}}" required="">
                                                    <option value="">Select Location</option>
                                                    @foreach($activity['hotel_list'] as $hotel)
                                                        <?php
                                                        $hotel_name = ($hotel['address'] != null ) ? $hotel['name'] ." , ". $hotel['address'] : $hotel['name'];
                                                        ?>
                                                        <option value="{{$hotel['id']}}" <?php if(@$hotelId == $hotel['id']) {?> selected <?php }?>>{{$hotel_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <label for="pickup-{{$activity['id']}}" generated="true" class="error"></label>
                                    </div>

                                    <?php 
                                    $displayl = "none";
                                    if(isset($hotelId) && $hotelId == "notListed")
                                    {
                                        $displayl = "block";
                                    }
                                    ?>

                                    <div class="col-12 col-sm-12 col-md-12 mt-3" id="notListed_{{$activity['id']}}" style="display: <?php echo $displayl ?>">
                                        <div class="fildes_outer">
                                            <label>Enter Pickup Location *</label>
                                            <input type="text" name="manual-pickup-{{$activity['id']}}" value="<?php echo @$activity['hotelPickups']['pickupPoint']; ?>" class="form-control manual-pickup-{{$activity['id']}}" required minlength="20">
                                        </div>
                                        <label for="manual-pickup-{{$activity['id']}}" generated="true" class="error"></label>
                                    </div>
                                                                        
                                    <div class="col-12 col-sm-3 col-md-12 mt-3">
                                        <div class="custom-control custom-radio">
                                            <input id="location7" name="location-select" type="radio" class="location-select custom-control-input" <?php echo (isset($hotelId)) ? '' : "checked"; ?> data-tourCode="{{$activity['id']}}">
                                            <label class="custom-control-label" for="location7"> I don't need to be picked up</label>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endif


                    @if(!empty($activity['bookingQuestions']))
                        <div class="mt-4 ml-2">
                            <div class="row">
                                @foreach($activity['bookingQuestions'] as $keys => $bookingq)
                                    @if($bookingq['questionId'] != 23)
                                        <div class="col-12 col-sm-3 col-md-3">
                                            <div class="fildes_outer">
                                                <label>{{$bookingq['title']}} *</label>
                                                <input type="text" class="form-control booking_{{$activity['id']}}" data-tourCode="{{$activity['id']}}" name="booking_{{$activity['id']}}_{{$bookingq['stringQuestionId']}}" data-question="{{$bookingq['questionId']}}" required aria-describedby="emailHelp" value="{{@$activity['bookingQuestionAnswers'][$keys]['answer'] }}">
                                            </div>
                                            <label for="booking_{{$activity['id']}}_{{$bookingq['stringQuestionId']}}" generated="true" class="error"></label>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if($activity['provider'] == "viator")

                    <div class="mt-2 ml-2">
                        <div class="row"> 
                            <div class="col-12">
                                <div class="fildes_outer my-3">
                                    <label>Special Requirements</label>
                                    <textarea name="special_req_{{$activity['id']}}" rows="4" class="form-control mt-3 special_req_{{$activity['id']}}" value="Special Requirements"><?php echo (isset($activity['specialRequirement'])) ? @$activity['specialRequirement'] : "";?></textarea>
                                </div>
                                <label for="special_req_{{$activity['id']}}" generated="true" class="error"></label>
                            </div>

                            <div class="p-2 my-4"> 
                                <?php /*<input type="hidden"  {{$activity['attributes']}} class="datetimepick" id="view_datepick-{{$activity['code']}}-{{$activity['id']}}" value="">
                                <input type="hidden"  class="datetimepick" id="datepick-{{ $activity['code']}}-{{$activity['id']}}" data-provider="{{$activity['provider']}}" data-city-id="{{$activity['cityid']}}" data-index="{{$code}}" data-activity-id="{{$code}}" data-is-selected="{{$select_act}}" data-price="{{$tourGrade['merchantNetPrice']}}"  data-price-id=""  data-name="{{$activity['activity_name']}}" data-currency="{{session()->get( 'search' )['currency']}}" data-startdate="{{$activity['startDate']}}" data-enddate="{{$activity['endDate']}}" data-description="{{$activity['description']}}" data-images="" data-duration="{{$activity['duration']}}" data-label="" data-bookdate = "{{$activity['bookingDate']}}" data-merchantNetPrice="{{$activity['tourGrade']['merchantNetPrice']}}" data-tour-code="{{$activity['id']}}" data-tourgrade-title="{{$activity['tourGrade']['gradeTitle']}}" data-adult="{{(isset($adultTotal)) ? $adultTotal : 0}}" data-child="{{(isset($childTotal)) ? $childTotal : 0}}" data-infant="{{(isset($infantTotal)) ? $infantTotal : 0}}" data-cancellation-policy="{{$activity['termsAndConditions']}}"  value="">*/?>
                                <button class="btn  btns_input_dark btn_update_iti d-block w-100" data-leg="{{$key}}" data-akey="{{$kact}}" data-code="{{$activity['id']}}"  data-weight-validate="{{$validate_weight}}" data-pickup="{{$hotelPickup}}">SUBMIT</button> 
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </form>
        </td>
    </tr>
    @if(isset($activity['tourGrades']['merchantNetPrice']))
      @php $i++; $j++; 
        $finalTotalCost = $finalTotalCost + $activity['tourGrades']['merchantNetPrice'];
      @endphp
    @elseif(isset($activity['price'][0]['price']))
      @php $i++; $j++; 
     
      $finalTotalCost = $finalTotalCost + (int) $activity['price'][0]['price'];
      @endphp
    @endif
  @endforeach

      @if(isset($activity['cancellation_policy']) && ($activity['cancellation_policy'] != ""))
      <div class="modal fade in" id="cancellationActivityPolicy{{$cj}}" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <div class="modal-header" style="border-bottom: none !important;padding: 15px 15px 0 15px;">
              <h4 class="modal-title" id="gridSystemModalLabel">Cancellation Policy</h4>
            </div>

              <div class="modal-body">
                  <div class="roomType-inner m-t-20">
                    <div class="m-t-20">
                      <p>{{@$activity['cancellation_policy']}}</p>
                    </div>

                    <div class="m-t-30 text-right">
                      <a href="#" data-dismiss="modal" class="modalLink-blue">CLOSE</a>
                    </div>
                  </div>
                </div>
            </div>
        </div> 
      </div>
    @endif

@else
  <tr>
    <td colspan="4">
      <div class="tour_icon cityboxIcon"><i class="ic-local_activity"></i> </div>
      <div class="tour_info cityboxDetails">
        <strong>Own Arrangement</strong>
      </div>
    </td>
  </tr>
@endif