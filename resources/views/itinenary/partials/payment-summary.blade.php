@extends('itinenary.booking-layout')

@section('booking-content')
@php
global $finalTotalCost;
$total = $totalCost * $travellers;
$adults = !empty (session()->get('search_input')['num_of_adults']) ?array_sum(session()->get('search_input')['num_of_adults']):0;
$children = !empty(session()->get('search_input')['num_of_children']) ? array_sum(session()->get('search_input')['num_of_children']):0;
if(!empty($adults) && !empty($children)){
    $total = (($adults  + $children) *$totalCost);
}if(!empty($adults) && empty($children)){
     $total = (($adults) *$totalCost);
}if(empty($adults) && !empty($children)){
     $total = (($children) *$totalCost);
}
$finalTotalCost = 0;
$travellers = session()->get('search')['travellers'];
$total_childs = session()->get('search')['child_total'];
$rooms = session()->get('search')['rooms'];
$eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
$tax_per = $licensee_account['tax']/100;

$passenger = session()->get('search_input')['pax'];

$paxDetailsArr = array();
if($passenger){
  foreach($passenger as $key => $value) {
    $j=0; $paxDetails = '';
    foreach($value as $pass) {
      $j++;
      $paxDetails .= ucwords($pass['firstname'].' '.$pass['lastname']); //.' ('.$pass['age'].')';
      if($pass['child'] == 1){
        if($pass['age'] == 0){ $pass['age'] = 1; }
        $paxDetails .= ' ('.$pass['age'].($pass['age'] > 1 ? ' Years':' Year').')';
      }
      if($j < count($value)) {$paxDetails .= ', '; }
    }
    $paxDetailsArr[$key] = $paxDetails;
  }
}

$bedTypeUpdate = 0;

@endphp

<style type="text/css">
  .qty_textbox_width {
      width: 210px !important;
      font-size: 15px !important;
  }
	form.itinerary_left{
		height:100%;
	}

  .badge {
    background-color: #ebebeb;
    color: #212121;
    padding: 0.4rem 1.2rem;
    border-radius: 20px;
    font-size: 1rem;
    font-weight: normal;
  }
  .fildes_outer label{ line-height: 14px; }
</style>

<div class="itinerary_right">
  @include('itinenary.partials.sidebarstrip')
  <div class="container-fluid">
    @include('itinenary.partials.payment-filter-option')
    <div class="itinerary_page pt-2">
      <div class="container-fluid">
        <div class="row">
             <div class="col-12">
                @if(session()->has('save_trip_success')) 
                <div class="alert alert-success" role="alert">
                    {{ session()->get('save_trip_success') }}
                </div>
                @endif
                @if(session()->has('save_trip_fail'))
                <div class="alert alert-danger" role="alert">
                    {{ session()->get('save_trip_fail') }}
                </div>
                @endif
                 <div class=" m-1">
                     <div class="tripDetails mb-3">
                         <div class="card panel border-0 rounded-0 p-3">
                             <div class="row">
                                 <div class="col-sm-7 col-xl-8">
                                     @if(!empty(session()->get('search_input')['option']) && (session()->get('search_input')['option'] == 'auto'))
                                      @if(!empty(session()->get('search')['itinerary']) && (count(session()->get('search')['itinerary'])>1) )
                                      <h5 class="font-weight-bold">Multi-City Tailormade Auto</h5>
                                      @else
                                      <h5 class="font-weight-bold">Single-City Tailormade Auto</h5>
                                      @endif
                                      @else
                                          <h5 class="font-weight-bold">Manual Itinerary</h5>
                                       @endif
                                     <div class="media">
                                         <i class="ic-calendar mr-2"></i>
                                         <div class="media-body pb-3 mb-0">
                                             {{$startDate}} - {{$endDate}}
                                         </div>
                                     </div>
                                 </div>
                                 <div class="col-sm-5 col-xl-4">
                                     <div class="row align-items-end">
                                         <div class="col-xl-9 col-sm-8 text-left text-sm-right">
                                             <ul class="list-unstyled mb-0">
                                                 <li class="list-inline-item">
                                                     <i class=" ic-local_hotel"></i>
                                                 </li>
                                                 <li class="list-inline-item">
                                                     <i class="ic-local_activity"></i>
                                                 </li>
                                                 <li class="list-inline-item">
                                                     <i class="ic-directions_bus"></i>
                                                 </li>
                                             </ul>
                                         </div>
                                         <div class="col-xl-3 col-sm-4 text-left text-sm-center mt-2 mt-md-0 border-left">
                                             <h2 class="font-weight-bold mb-0">
                                              <?php $daysCount = explode(' ', $totalDays) ;
                                                echo !empty($daysCount[0]) ? $daysCount[0]:$totalDays;
                                              ?>
                                             </h2>
                                             <?php echo ($totalDays>1) ? 'Days' :'Day'; ?>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
        </div>
        @php
        $cj = 1;
        $i = 1;
        $activities = '';
        $transport = '';
        $hotel = '';
        $total_pax = session()->get('search')['travellers'];
        $adults = array_sum(session()->get('search_input')['num_of_adults']);
        $children = array_sum(session()->get('search_input')['num_of_children']);
        $total_traveller =  $adults + $children;
        $rooms = session()->get('search')['rooms'];
        $itineraryNumber = 0;
        $total_leg = count(session()->get('search')['itinerary']) - 1;
        @endphp
          <div class="row">
            <div class="col-12">
              <div class="save_itinerary  m-1">
                  @if (session()->get('search'))
                    @foreach (session()->get('search')['itinerary'] as $key => $leg)
                      <div class="itinerary_list p-3 pb-1 <?php if($key != 0) { echo 'mt-4'; } ?>">
                        <table class="table mb-0 mt-2 p-0" width="100%">
                          <tr>
                            <th class="border-top-0 border-bottom" width="40%">{{$leg['city']['name']}}</th>
                            
                              <th class="border-top-0 border-bottom text-center" width="20%">Cost</th>
                              <th class="border-top-0 border-bottom text-center" width="20%">Quantity</th>
                              <th class="border-top-0 border-bottom text-right" width="20%">Total Cost</th>
                            <?php /*@if($key == 0) 
                            @else
                              <th class="border-top-0 border-bottom text-center" width="20%"></th>
                              <th class="border-top-0 border-bottom text-center" width="20%"></th>
                              <th class="border-top-0 border-bottom text-right" width="20%"></th>
                            @endif*/ ?>
                          </tr>

                          <!-- Accommodation Details Start -->
                            @include('itinenary.partials.payment-summary-hotel')
                          <!-- Accommodation Details End -->

                          <!-- Activity Details Start -->
                            @include('itinenary.partials.payment-summary-activity')
                          <!-- Activity Details End -->

                          <!-- Transport Details Start -->
                            @include('itinenary.partials.payment-summary-transport')
                          <!-- Transport Details End -->
                        </table>

                        <input type="hidden" id="bedTypeUpdate" value="<?php echo $bedTypeUpdate;?>">
                      </div>
                        
                      @php $itineraryNumber++; $cj++; @endphp
                    @endforeach
                  @endif

                  <table class="table mb-0">
                    <thead>
                           <tr>
                               <td>

                                   <div class="pl-5">
                                      <p> <a href="javascript:void(0)" class="disable_item_custom">Save Itinerary</a></p>

                                       <p> <a href="javascript:void(0)" class="disable_item_custom">View Saved Itineraries</a></p>

                                       <p> <a href="javascript:void(0)" class="disable_item_custom">Enter Promo Code</a> </p>

                                       <p class="pt-2">Contact Us <a href="javascript:void(0)" class="disable_item_custom">+61 (0)3 9999 6774 </a></p>

                                   </div>
                               </td>
                               <td colspan="2" class="text-right">
                                   <!-- <p>Total Per Person</p> -->
                                   <p>Sub Total Amount</p>
                                   <p>Credit Card Fee 2.5%</p>
                                   <p>Tax {{ $tax_per * 100 }}%</p>
                                   <p class="pt-2"><strong>Total Amount</strong></p>
                                   <input type="hidden" id="tax_per" value="{{$tax_per}}">
                               </td>
                              @php 
                                $gst = $finalTotalCost * 0.025;
                                $t = $finalTotalCost + $gst;
                                $tax = $t * $tax_per; 
                                $finalTotal = number_format($finalTotalCost + $gst + $tax, 2, '.', ',');
                              @endphp
                               <td class="text-right">
                                   <!-- <p>${{$currency}} {{number_format($finalTotalCost, 2, '.', ',')}}</p> -->
                                   <p>${{$currency}} {{number_format($finalTotalCost, 2, '.', ',')}}</p>
                                   <p>${{$currency}} {{number_format($gst, 2, '.', ',')}}</p>
                                   <p>${{$currency}} {{number_format($tax, 2, '.', ',')}}</p>
                                   <p class="pt-2"><strong>$AUD {{$finalTotal}}</strong></p>

                                   <?php     
                                      session()->put('finalTotalCost',$finalTotalCost);
                                      session()->put('gst',$gst);
                                      session()->put('tax',$tax);
                                    ?>
                               </td>
                           </tr>
                    </tbody>
                  </table>
                  <div class="p-2"> <!-- {{url('/signin-guest-checkout/')}} -->
                    <a href="#" data-location="{{url('/signin-guest-checkout/')}}" class="btn  btns_input_dark transform saveInstruction d-block w-100">CHECKOUT</a>
                  </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push('scripts')
   <script src="{{url('js/itinerary/common.js')}}"></script>
   <script src="{{url('js/booking-summary.js')}}"></script>
   <script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
   <script type="text/javascript">

      // if flights are no longer available
     var isFlightAvailable = "{{$isFlightAvailable}}";
     if(isFlightAvailable == 1){
      
      $('.no-flight-summary').modal('show');

     }
    $(document).on('click','.baggage_fare_rules',function() {
        var FareSourceCode = $(this).attr('data-fare');
            eroam.ajax('post', 'fareRules', {
            FareSourceCode: FareSourceCode
            }, function(response) {
                $('#fareRuleModal').html(response);
                $('#info-modal').modal('show');

                $('.baggage-content').slimScroll({
                  height: '380px',
                  color: '#212121',
                  opacity: '0.7',
                  size: '5px',
                  allowPageScroll: true
               });  
                $('.baggage-left-content').slimScroll({
                  height: '380px',
                  color: '#212121',
                  opacity: '0.7',
                  size: '5px',
                  allowPageScroll: true
               });  


            }, function() {
                $('#fareRuleModal').html(''); 
                $(".loader").show();
            }, function() {
                $(".loader").hide();
            });
      });

     $(document).on('click','.extraServiceId',function() {
        var basePrice = $('#baseFLightPriceID').val();
        var servicePrice = $(this).closest('.form-group').find('.extraServiceClass').val();
        var total = 0;
        if (!$(this).is(':checked')) {
            var total =  parseFloat(basePrice) - parseFloat(servicePrice);
            total = Math.abs(total).toFixed(2);
        }else{
          var total =  parseFloat(basePrice) + parseFloat(servicePrice);
          total = Math.abs(total).toFixed(2);
        }
        $('#baseFLightPriceID').val(total);
        $('.baseFLightPrice').text(total);
    });

    $(document).on('click','.btn_update_extra_service',function(e) {
       e.preventDefault();
       var leg = $(this).attr('data-leg');
       var baseFLightPrice = $('#baseFLightPriceID').val();
       extraServiceId = [];
       $('.extraServiceId').each(function(){
          if ($(this).is(':checked')) {
            extraServiceId.push($(this).val());
          }
       });
       if(extraServiceId){
        var search_session = JSON.parse( $('#search-session').val() );
        var transport_data = search_session.itinerary[leg].transport;
        transport_data.price[0].price = baseFLightPrice;
        transport_data.serviceId = extraServiceId;
        $(".loader").show();
            bookingSummary.update(JSON.stringify(search_session));  
            
            $(document).ajaxStop(function() {
                $(".loader").hide();
                var session_search = JSON.stringify( search_session );                
                $('#search-session').val(session_search);
                location.reload();
                //parent.location.reload();
            });
      }
    }); 
  </script>  
 @endpush
