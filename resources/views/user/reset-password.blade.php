@extends('layouts.home')
@section('content')
<div class="body_sec">
    <div class="blue modal_outer_man">
        @include('partials.banner')
        @include('partials.search')
        @include('partials.resetpassword')
    </div>
</div>
@endsection


@push('scripts')
  <script type="text/javascript" src="{{ url('js/resetpassword.js') }}"></script>
@endpush
