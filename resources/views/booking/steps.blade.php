<div id="booking-steps">
	<div class="booking-step-container {{ request()->segment(2) == 'pax-information' ? 'active' : '' }}">
		<span><i class="flaticon-backpacker"></i> Pax Information</span>
	</div>
	<div class="booking-step-container {{ request()->segment(2) == 'confirm' ? 'active' : '' }}">
		<span><i class="flaticon-business"></i> Payment</span>
	</div>
	<div class="booking-step-container {{ request()->segment(2) == 'review' ? 'active' : '' }}">
		<span><i class="flaticon-approve-circular-button"></i> Confirm</span>
	</div>
</div>