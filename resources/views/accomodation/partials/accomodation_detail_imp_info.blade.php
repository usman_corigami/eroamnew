@if($data->provider=="expedia")
	<p>{{$data->hotelName}} <br> {{$data->country_name}}, {{$data->searchCity}} / {{$data->hotelCity}} </p>
	<div class="row">
	    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
	        <div class="pb-4">
	            <p class="mb-2"><strong>@lang('home.hotel_amenities_text') </strong></p>
	            <div class="amenities">
					<div class="row">
						@if($data->PropertyAmenities->$size > 1)
						<div class="col-sm-6">
							<ul class="pl-4 pt-0">
								@if($data->PropertyAmenities->PropertyAmenity)
									@foreach ($data->PropertyAmenities->PropertyAmenity as $key => $value)
									   @if ($key % 2 == 0)
										<li class="pr-5">{{$value->amenity}}</li>
									   @endif
									@endforeach
								@endif
							</ul>
						</div>
						<div class="col-sm-6">
							<ul class="pl-4 pt-0">
								@if($data->PropertyAmenities->PropertyAmenity)
									@foreach ($data->PropertyAmenities->PropertyAmenity as $key => $value)
									   @if ($key % 2 != 0)
										<li class="pr-5">{{$value->amenity}}</li>
									   @endif
									@endforeach
								@endif
							</ul>
						</div>
						@else 
							<div class="col-sm-6">
								<ul class="pl-4 pt-0">
									@if($data->PropertyAmenities->PropertyAmenity)
										<li class="pr-5">{{$data->PropertyAmenities->PropertyAmenity->amenity}}</li>
									@endif
								</ul>
							</div>
						@endif
					</div>
	            </div>
	            <div class="clearfix"></div>
	        </div>
			@if(isset($data->HotelDetails->diningDescription))
	        <div class="pb-4">
	            <p class="mb-2"><strong>@lang('home.hotel_detials_label1') </strong></p>
	            <div class="amenities">
	                <p>{{$data->HotelDetails->diningDescription}}</p>
	            </div>
	            <div class="clearfix"></div>
	        </div>
	        @endif
	    </div>
	    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
	        @isset($data->checkInInstructions)
	        <div class="pb-4">
	            <p class="mb-2"><strong>@lang('home.hotel_checkin_instruction_text') </strong></p>
				<div class="info_tab_desc pl-4">
					<p>
						@php
							$data->checkInInstructions = strstr($data->checkInInstructions, '<p><b>Fees</b>');
							$data->checkInInstructions = str_replace('<p><b>Fees</b>','<p><strong>Fees / Optional Extras</strong>',$data->checkInInstructions);
						@endphp
						{!!html_entity_decode($data->checkInInstructions)!!}
					</p>
				</div>
	            <div class="clearfix"></div>
	        </div>
	        @endisset

	        @isset($data->specialCheckInInstructions)
	        <div class="pb-4">
	            <p class="mb-2"><strong>Special Check In Instructions</strong></p>
				<div class="info_tab_desc pl-4">
					<p>{!!html_entity_decode($data->specialCheckInInstructions)!!}</p>
				</div>
	            <div class="clearfix"></div>
	        </div>
	        @endisset
	    </div>
	</div>
@elseif($data->provider == 'eroam')
	<p>{{$data->hotelName}} <br> {{$data->country_name}}, {{$data->searchCity}}  {{($data->hotelCity) ? '/'.$data->hotelCity : ''}} </p>
	<div class="row">
	    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
	    	@if($data->amenities_description != '')
		        <div class="pb-4">
		            <p class="mb-2"><strong>@lang('home.hotel_amenities_text') </strong></p>
		            <div class="amenities">
						<div class="row">
							<div class="col-sm-6">
								{!! $data->amenities_description !!}
							</div>
						</div>
		            </div>
		            <div class="clearfix"></div>
		        </div>
	        @endif
	    </div>
	    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
	        @if($data->checkin_instruction != '')
	        <div class="pb-4">
	            <p class="mb-2"><strong>@lang('home.hotel_checkin_instruction_text') </strong></p>
				<div class="info_tab_desc pl-4">
					<p>
						{!!html_entity_decode($data->checkin_instruction)!!}
					</p>
				</div>
	            <div class="clearfix"></div>
	        </div>
	        @endif
	    </div>
	</div>
	
@endif