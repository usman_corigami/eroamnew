<!-- START ALERT -->
<div class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="sessionExpire" id="session-expire">
    <div class="modal-dialog modal-sm" role="document" >
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none !important;padding: 15px 15px 0 15px;">
                <h4 class="modal-title" id="gridSystemModalLabel">Session Notification</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="">
                        Your eRoam session is about to time-out. Would you like to continue?
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="background: none;">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8 text-center">
                        <button type="button" class="btn btn-primary session-home-btn">Yes</button>
                        <button type="button" class="btn btn-primary session-continue-btn"> No</button>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->