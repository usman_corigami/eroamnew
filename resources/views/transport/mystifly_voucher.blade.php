<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $filename }} :: eRoam</title>
    <style type="text/css">
      .page-break {
          page-break-after: always;
      }
      body{
        font-family: Arial;
        color: #212121;
        font-size: 14px;
        margin: 0px;
        padding: 0px;
      }
      p{
        margin: 5px 0;
      }
      ul li{ padding: 4px 0; }
      h2{margin: 5px 0;}
      table tr th{margin-bottom: 30px;}
    </style>
  </head>
  <body>
    @if(isset($detailArray['data']))
    @php  
      //echo "<pre>";print_r($detailArray['FareRules']['rules']);exit;
      $FlightCode = getCityAirportCode();
      $detailArray['success'] = 1;
      if(!isset($detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ReservationItems']['ReservationItem'][0]))
      {
        $data = $detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ReservationItems']['ReservationItem'];
        unset($detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ReservationItems']['ReservationItem']);
        $detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ReservationItems']['ReservationItem'][0] = $data;

      }
      if(!isset($detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['CustomerInfos']['CustomerInfo'][0]))
      {
        $data = $detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['CustomerInfos']['CustomerInfo'];
        unset($detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['CustomerInfos']['CustomerInfo']);
        $detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['CustomerInfos']['CustomerInfo'][0] = $data;
      }

      $logo = getDomainLogo();
      if(!$logo) {
          $logo = public_path('/assets/images/flight-white.png');
      }
    @endphp
    <table width="100%">
      <tbody>
        <!-- <tr>
          <td>
            <img src="makemytrip-logo.png" alt="eRoam" style="width: 180px;">
          </td>
          <td style="text-align: right;">
            <h2 style="margin-top: 10px;"><img src="flight.svg" alt="" /> Flight Confirmation Voucher</h2>
          </td>
        </tr> -->
        <tr>
          <td colspan="2">
            <table width="100%" style="background-color: #212121; color: #fff;">
              <tr>
                <td style="padding: 0 10px;">
                  <img src="{{ $logo }}" alt="eRoam" style="width: 100px;">
                </td>
                <td style="text-align: right; padding: 3px 15px;">
                  <h2><img src="{{ public_path('/assets/images/flight-white.png') }}" alt="eRoam" style="position: relative; top: 6px; margin-top: 0px; width: 25px;"> Flight Confirmation Voucher</h2>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
          	@if(isset($detailArray) && $detailArray['success'])
          	<?php $passengerName =  $detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['CustomerInfos']['CustomerInfo'][0]['Customer']['PaxName']['PassengerTitle'].' '.$detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['CustomerInfos']['CustomerInfo'][0]['Customer']['PaxName']['PassengerFirstName'].' '.$detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['CustomerInfos']['CustomerInfo'][0]['Customer']['PaxName']['PassengerLastName']; ?>
            <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th colspan="4">
                    <h2 style="margin: 8px 8px 8px 2px;">Passenger and Ticket Information</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><strong>Passenger Name</strong></td>
                  <td>{{$passengerName}}</td>
                  <td><strong>Booking ID</strong></td>
                  <td>{{ $detailArray['booking_id'] ?? '' }}</td>
                </tr>
                <tr>
                    <td><strong>Unique ID</strong></td>
                    <td>{{ $detailArray['data']['TripDetailsResult']['TravelItinerary']['UniqueID']}}</td>
                  </tr>
              </tbody>
            </table>
            @endif
          </td>
        </tr>
        <tr>
          <td colspan="2">
              <table width="100%" border="1" style="border: solid 3px #fafafa; text-align: center; border-collapse: collapse; margin-top: 5px;" cellspacing="4" cellpadding="10">
              <tr>
                <td>
                  <h3 style="margin: 0;">Booking Date</h3>
                  <p>@php echo date('jS M Y @ h:i'); @endphp</p>
                </td>
                <td>
                  <h3 style="margin: 0;">Departure</h3>
                  <p>({{ $FlightCode[$detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ReservationItems']['ReservationItem'][0]['DepartureAirportLocationCode']] ?? $detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ReservationItems']['ReservationItem'][0]['DepartureAirportLocationCode'] }})</p>
                </td>
                <td>
                    <h3 style="margin: 0;">Arrival</h3> 
                  <p>({{$FlightCode[$detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ReservationItems']['ReservationItem'][0]['ArrivalAirportLocationCode']] ?? $detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ReservationItems']['ReservationItem'][0]['ArrivalAirportLocationCode'] }})</p>
                </td>
                <td>
                  <h3 style="margin: 0;">Passenger</h3>
                  <p>{{count($detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['CustomerInfos']['CustomerInfo']) ?? 0}}</p>
                </td>
                <td>
                  <h3 style="margin: 0;">Stop</h3>
                  <p>{{ count($detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ReservationItems']['ReservationItem'])-1 ?? ''}}</p>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
            <td colspan="2">
              <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px; border-color: #fafafa;" cellspacing="4" cellpadding="6">
                <thead>
                  <tr style="background-color: #fafafa;">
                    <th colspan="7">
                      <h2 style="margin: 8px 8px 8px 2px;">Itinerary &amp; Reservation Details</h2>
                    </th>
                  </tr>
                </thead>
                <tbody>
                	@foreach($detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ReservationItems']['ReservationItem'] as $ReservationItem)
                  <tr>
                    <td style="text-align: center; border:solid 1px #fafafa;">
                        <img src="{{ public_path('/assets/images/flight.png') }}" />
                       <!-- http://pics.avs.io/100/50/{{ $ReservationItem['MarketingAirlineCode'] }}.png -->
                      <br>
                      <p><strong>{{$ReservationItem['FlightNumber'] }}</strong></p> 
                    </td>
                    <td style="border:solid 1px #fafafa; padding-left: 20px; text-align: center;">
                      <strong>Departure</strong> <br>
                      <p>({{ $FlightCode[$ReservationItem['DepartureAirportLocationCode']] ?? $ReservationItem['DepartureAirportLocationCode'] }})</p>{{ date('d M Y @ h:i', strtotime($ReservationItem['DepartureDateTime'])) }}
                    </td>
                    <td style="border:solid 1px #fafafa; padding-left: 20px; text-align: center;">
                      <strong>Arrival</strong> <br>
                      <p>({{$FlightCode[$ReservationItem['ArrivalAirportLocationCode']] ?? $ReservationItem['ArrivalAirportLocationCode'] }})</p>
                      {{ date('d M Y @ h:i', strtotime($ReservationItem['ArrivalDateTime'])) }}
                    </td>
                    <td style="border:solid 1px #fafafa; text-align: center;">
                      <strong>Class</strong><br>

                      <p>{{ ($ReservationItem['CabinClassText']!= '') ? $ReservationItem['CabinClassText'] : 'Economy'}}</p>
                    </td>
                    <td style="border:solid 1px #fafafa; text-align: center;">
                      <strong>Airline PNR</strong>
                      <p>{{$ReservationItem['AirlinePNR'] ?? ''}}</p>
                    </td>
                    <td style="border:solid 1px #fafafa; text-align: center;">
                      <strong>Duration</strong>
                      <p>{{ $ReservationItem['JourneyDuration']?? '' }}</p>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="7" style="padding: 0px;">
                      <table width="100%" style="border:solid 1px #fafafa; border-collapse: collapse;" cellpadding="6">
                        <thead>
                          <tr>
                            <th style="border:solid 1px #fafafa; padding: 10px 10px 10px 20px;"><strong>Passenger Name</strong></th>
                            <th style="border:solid 1px #fafafa; padding: 10px 10px 10px 20px;"><strong>Type</strong></th>
                            <th style="border:solid 1px #fafafa; padding: 10px 10px 10px 20px;">E-Ticket Number</th>
                          </tr>
                        </thead>
                        <tbody>
                        	@foreach($detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['CustomerInfos']['CustomerInfo'] as $CustomerInfo)
                          <tr>
                            <td style="border:solid 1px #fafafa; padding: 10px 10px 10px 20px;">{{$CustomerInfo['Customer']['PaxName']['PassengerFirstName'] ?? ''}} {{$CustomerInfo['Customer']['PaxName']['PassengerLastName'] }}</td>
                            <td style="border:solid 1px #fafafa; padding: 10px 10px 10px 20px;">{{$CustomerInfo['Customer']['PassengerType'] }}</td>
                            <td style="border:solid 1px #fafafa; padding: 10px 10px 10px 20px;">{{$CustomerInfo['Customer']['NameNumber'] }}</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="7" style="padding: 5px;">
                      <hr>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th colspan="2">
                    <h2 style="margin: 8px 8px 8px 2px;">Fare &amp; Additional Information</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                  <tr>
                    <td colspan="2">
                      <table width="50%;" cellpadding="4">
                        <tr>
                          <td width="30%">Fare</td>
                          <td style="text-align: right; padding-right: 20px;">{{$detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ItineraryPricing']['EquiFare']['CurrencyCode']}} {{$detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ItineraryPricing']['EquiFare']['Amount'] ?? ''}}</td>
                        </tr>
                       
                        <tr>
                          <td width="30%">Taxes</td>
                          <td style="text-align: right; padding-right: 20px;">{{$detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ItineraryPricing']['Tax']['CurrencyCode']}} {{$detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ItineraryPricing']['Tax']['Amount'] ?? ''}}</td>
                        </tr>
                        <tr>
                          <td width="30%"><strong>Total Fare</strong></td>
                          <td style="text-align: right; padding-right: 20px;"><strong>{{$detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ItineraryPricing']['TotalFare']['CurrencyCode']}} {{$detailArray['data']['TripDetailsResult']['TravelItinerary']['ItineraryInfo']['ItineraryPricing']['TotalFare']['Amount'] ?? ''}}</strong></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
              </tbody>
            </table>
          </td>
        </tr>
        @if(isset($detailArray['FareRules']) && isset($detailArray['FareRules']['rules']) && count($detailArray['FareRules']['rules']))
        <tr>
          <td colspan="2">
            <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th colspan="3">
                    <h2 style="margin: 8px 8px 8px 2px;">Baggage and Fare Rules</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                      <?php $count = 0; $Wordcount = 1000;?>
                      @foreach($detailArray['FareRules']['rules'] as $key => $FareRule)
                        <?php $count += strlen($detailArray['FareRules']['category'][$key]) + strlen(nl2br($FareRule)); 
                          $localcount = strlen($detailArray['FareRules']['category'][$key]) + strlen(nl2br($FareRule));
                        ?>
                        @if($count > $Wordcount || $localcount > $Wordcount) <?php $count = $localcount; ?><div class="page-break"></div> @endif
                        @if($localcount > $Wordcount)
                          <h3 style="margin: 25px 0 8px 0;">{{ $detailArray['FareRules']['category'][$key] ?? ''}}</h3>
                          <p>
                          {!! wordwrap( $FareRule, 3000 ,'<div class="page-break"></div>') !!}
                         </p>
                        @else
                          <h3 style="margin: 25px 0 8px 0;">{{ $detailArray['FareRules']['category'][$key] ?? ''}}</h3>
                          <p>{!! $FareRule !!}</p><br/>
                        @endif

                      @endforeach
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        @endif
        <tr>
          <td>
            <p style="margin-top: 40px; font-size: 16px;">We wish you a hassle-free, Happy journey.</p>
              
            <p style="margin-top: 20px; font-size: 16px;">Thank you,<br>
              <strong>Team eRoam</strong></p>
          </td>
        </tr>
      </tbody>
    </table>
    @else
      echo "<pre>";print_r(json_encode($detailArray));echo "</pre>";exit;
    @endif
  </body>
</html>